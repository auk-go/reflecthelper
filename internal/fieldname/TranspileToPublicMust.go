package fieldname

import "unicode"

func TranspileToPublicMust(fieldName string) string {
	if len(fieldName) == 0 {
		return ""
	}

	fieldNameRune := []rune(fieldName)
	// make the first char uppercase
	fieldNameRune[0] = unicode.ToUpper(fieldNameRune[0])

	return string(fieldNameRune)
}
