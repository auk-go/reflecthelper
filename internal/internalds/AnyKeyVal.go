package internalds

type AnyKeyVal struct {
	Key      string
	AnyValue interface{} // reflection package is different, here data can be interfaces
}
