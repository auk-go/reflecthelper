package refinternal

import (
	"reflect"
	"unsafe"
)

type valueGetter struct{}

func (it *valueGetter) GetPublicValue(rv reflect.Value) interface{} {
	if !rv.IsValid() {
		return nil
	}

	k := rv.Kind()

	if k == reflect.Ptr || k == reflect.Interface {
		return rv.Elem().Interface()
	}

	return rv.Interface()
}

func (it *valueGetter) GetStructField(
	anyStruct interface{},
	fieldName string,
) reflect.StructField {
	rVal := reflect.ValueOf(anyStruct)
	f, _ := rVal.Type().FieldByName(fieldName)

	return f
}

func (it *valueGetter) GetPrivateFieldValue(
	rv reflect.Value,
) interface{} {
	unexportedField := reflect.NewAt(
		rv.Type(),
		unsafe.Pointer(rv.UnsafeAddr())).
		Elem()

	return unexportedField.Interface()
}
