package refinternal

import (
	"errors"
	"reflect"
	"unsafe"

	"gitlab.com/auk-go/reflecthelper/internal/consts"
	"gitlab.com/auk-go/reflecthelper/reflectmodel"
)

type looper struct{}

func (it *looper) FieldsFor(
	anyItem interface{},
	processor func(currentField *reflectmodel.FieldProcessor) (err error),
) error {
	rv := reflect.ValueOf(anyItem)

	return it.FieldsForRv(rv, processor)
}

func (it *looper) FieldsForRv(
	rv reflect.Value,
	processor func(currentField *reflectmodel.FieldProcessor) (err error),
) error {
	reduceRv := it.ReducePointerRvDefault(rv)

	if reduceRv.IsInvalid() || reduceRv.HasError() {
		return reduceRv.Error
	}

	// valid
	structType := reduceRv.FinalReflectVal.Type()
	fieldsLength := structType.NumField()

	for i := 0; i < fieldsLength; i++ {
		field := structType.Field(i)
		input := reflectmodel.FieldProcessor{
			Name:      field.Name,
			Index:     i,
			Field:     field,
			FieldType: field.Type,
		}

		e := processor(&input)

		if e != nil {
			return e
		}
	}

	return nil
}

func (it *looper) FieldNames(
	anyStruct interface{},
) (fieldNames []string, err error) {
	rv := reflect.ValueOf(anyStruct)

	return it.FieldNamesRv(rv)
}

func (it *looper) FieldNamesRv(
	rv reflect.Value,
) (fieldNames []string, err error) {
	reduceRv := it.ReducePointerRvDefault(rv)

	if reduceRv.IsInvalid() || reduceRv.HasError() {
		return []string{}, reduceRv.Error
	}

	// valid
	structType := reduceRv.FinalReflectVal.Type()
	fieldsLength := structType.NumField()

	for i := 0; i < fieldsLength; i++ {
		field := structType.Field(i)
		fieldNames = append(fieldNames, field.Name)
	}

	return fieldNames, nil
}

func (it *looper) FieldsMap(
	anyItem interface{},
) (resultsMap map[string]*reflect.StructField, err error) {
	rv := reflect.ValueOf(anyItem)

	return it.FieldsMapRv(rv)
}

func (it *looper) FieldsMapRv(
	rv reflect.Value,
) (resultsMap map[string]*reflect.StructField, err error) {
	reduceRv := it.ReducePointerRvDefault(rv)

	if reduceRv.IsInvalid() || reduceRv.HasError() {
		return map[string]*reflect.StructField{}, reduceRv.Error
	}

	// valid
	structType := reduceRv.FinalReflectVal.Type()
	fieldsLength := structType.NumField()
	resultsMap = make(
		map[string]*reflect.StructField,
		fieldsLength)

	for i := 0; i < fieldsLength; i++ {
		field := structType.Field(i)
		resultsMap[field.Name] = &field
	}

	return resultsMap, nil
}

func (it *looper) MethodsMap(
	anyItem interface{},
) (resultsMap map[string]*reflect.Method, err error) {
	rv := reflect.ValueOf(anyItem)

	return it.MethodsMapRv(rv)
}

// ReducePointer
//
//	level -1 means all levels (****...) to Non pointer
func (it *looper) ReducePointer(
	anyItem interface{},
	level int,
) *reflectmodel.ReflectValueKind {
	return it.ReducePointerRv(reflect.ValueOf(anyItem), level)
}

func (it *looper) ReducePointerDefault(
	anyItem interface{},
) *reflectmodel.ReflectValueKind {
	return it.ReducePointerRv(reflect.ValueOf(anyItem), defaultPointerReduction)
}

func (it *looper) ReducePointerRvDefault(
	reflectVal reflect.Value,
) *reflectmodel.ReflectValueKind {
	return it.ReducePointerRv(reflectVal, defaultPointerReduction)
}

// ReducePointerRv
//
//	level -1 means all levels (****...) to Non pointer
func (it *looper) ReducePointerRv(
	reflectVal reflect.Value,
	level int,
) *reflectmodel.ReflectValueKind {
	structValueKind := reflectVal.Kind()
	hasLevel := level > consts.Invalid
	structValue := reflectVal

	// reducing ****ToValue to ToValue
	for structValueKind == reflect.Ptr || structValueKind == reflect.Interface {
		// mutating dangerous code
		structValue = structValue.Elem()
		structValueKind = structValue.Kind()

		level--
		if hasLevel && level <= 0 {
			break
		}
	}

	if !structValue.IsValid() || structValueKind != reflect.Struct {
		return reflectmodel.InvalidReflectValueKindModel(
			"invalid ref value or could not reach in level limit")
	}

	// valid
	return &reflectmodel.ReflectValueKind{
		IsValid:         true,
		FinalReflectVal: structValue,
		Kind:            structValue.Kind(),
		Error:           nil,
	}
}

func (it *looper) MethodsFor(
	anyItem interface{},
	processor func(
		totalMethodsCount int,
		method *reflectmodel.MethodProcessor,
	) (err error),
) error {
	// valid
	// https://stackoverflow.com/q/598defaultPointerReduction1642
	// https://prnt.sc/kmkTmVmO2cPH
	// Pointer connected method and non pointer connect methods will be different
	rv := reflect.ValueOf(anyItem) // can be a pointer or non pointer

	return it.MethodsForRv(rv, processor)
}

func (it *looper) MethodNamesRv(
	rv reflect.Value,
) (methodNames []string, err error) {
	reduceRv := it.ReducePointerRvDefault(rv)

	if reduceRv.IsInvalid() || reduceRv.HasError() {
		return methodNames, reduceRv.Error
	}

	// valid
	structType := rv.Type()
	fieldsLength := structType.NumField()

	for i := 0; i < fieldsLength; i++ {
		field := structType.Field(i)
		methodNames = append(methodNames, field.Name)
	}

	return methodNames, nil
}

// func (it *looper) SliceForRv(
// 	reflectVal reflect.Value,
// 	processor func(
// 		totalMethodsCount int,
// 		method *reflectmodel.MethodProcessor,
// 	) (err error),
// ) error {
// 	if reflectVal.Kind() == reflect.Ptr {
// 		return it.SliceForRv(reflect.Indirect(reflect.ValueOf(reflectVal)), processor)
// 	}
//
// 	if reflectVal.Kind() != reflect.Slice {
// 		return errors.New("SliceForRv Reflection is not a slice")
// 	}
//
// 	mapKeys := reflectVal.MapKeys()
// 	length := len(mapKeys)
// 	keys := make([]string, length)
//
// 	for i, key := range reflectVal.MapKeys() {
// 		keyAny := key.Interface()
// 		keyAsString, isString := keyAny.(string)
//
// 		if !isString {
// 			return keys, errcore.TypeMismatchType.Error("Not string type", keyAny)
// 		}
//
// 		keys[i] = keyAsString
// 	}
//
// 	return keys, nil
// }

func (it *looper) MethodsForRv(
	rv reflect.Value,
	processor func(
		totalMethodsCount int,
		method *reflectmodel.MethodProcessor,
	) (err error),
) error {
	// valid
	// https://stackoverflow.com/q/598defaultPointerReduction1642
	// https://prnt.sc/kmkTmVmO2cPH
	// Pointer connected method and non pointer connect methods will be different
	ptrRv, conErr := it.ToPointerReflectValueRv(rv)

	if conErr != nil {
		return conErr
	}

	err := it.loopBaseMethods(ptrRv, processor)

	if err != nil {
		return err
	}

	// non pointer
	reducer := it.ReducePointerRvDefault(rv)

	return it.loopBaseMethods(reducer.FinalReflectVal, processor)
}

func (it *looper) MethodsMapRv(
	rv reflect.Value,
) (map[string]*reflect.Method, error) {
	// valid
	// https://stackoverflow.com/q/598defaultPointerReduction1642
	// https://prnt.sc/kmkTmVmO2cPH
	// Pointer connected method and non pointer connect methods will be different
	ptrRv, conErr := it.ToPointerReflectValueRv(rv)

	if conErr != nil {
		return map[string]*reflect.Method{}, conErr
	}

	resultsMap := it.baseMethodsMap(ptrRv)

	// non pointer
	reducer := it.ReducePointerRvDefault(rv)

	resultsMapNext := it.baseMethodsMap(
		reducer.FinalReflectVal)

	for s, method := range resultsMapNext {
		resultsMap[s] = method
	}

	return resultsMap, nil
}

// ToPointerReflectValue
//
// anyItem must be a struct or pointer to struct
func (it *looper) ToPointerReflectValue(
	anyItem interface{},
) (reflect.Value, error) {
	// valid
	// https://stackoverflow.com/q/598defaultPointerReduction1642
	// https://prnt.sc/kmkTmVmO2cPH
	// Pointer connected method and non pointer connect methods will be different
	rv := reflect.ValueOf(anyItem) // can be a pointer or non pointer

	return it.ToPointerReflectValueRv(rv)
}

// ToPointerReflectValueRv
//
// Rv must be a struct or pointer to struct
func (it *looper) ToPointerReflectValueRv(
	rv reflect.Value,
) (reflect.Value, error) {
	// valid
	// https://stackoverflow.com/q/598defaultPointerReduction1642
	// https://prnt.sc/kmkTmVmO2cPH
	// Pointer connected method and non pointer connect methods will be different
	k := rv.Kind()
	switch k {
	case reflect.Ptr:
		return rv, nil
	case reflect.Struct:
		toInterface := rv.Interface()
		toPointer := &toInterface
		unsafePtr := unsafe.Pointer(&toPointer)

		return reflect.NewAt(rv.Type(), unsafePtr), nil
	}

	return reflect.Value{}, errors.New("pointer and Struct is only allowed - given type - " + k.String())
}

// loopBaseMethods
//
// Pointer and non pointer methods are attached differently.
// Call this twice
func (it *looper) loopBaseMethods(
	rv reflect.Value, // can be a pointer or non pointer
	processor func(
		totalMethodsCount int,
		method *reflectmodel.MethodProcessor,
	) (err error),
) error {
	// valid
	// https://stackoverflow.com/q/598defaultPointerReduction1642
	// https://prnt.sc/kmkTmVmO2cPH
	// Pointer connected method and non pointer connect methods will be different
	structType := rv.Type()
	methodsCount := rv.NumMethod()

	for i := 0; i < methodsCount; i++ {
		method := structType.Method(i)
		input := reflectmodel.MethodProcessor{
			Name:          method.Name,
			Index:         i,
			ReflectMethod: method,
		}

		e := processor(methodsCount, &input)

		if e != nil {
			return e
		}
	}

	return nil
}

// loopBaseMethods
//
// Pointer and non pointer methods are attached differently.
// Call this twice
func (it *looper) baseMethodsMap(
	rv reflect.Value, // can be a pointer or non pointer
) map[string]*reflect.Method {
	// valid
	// https://stackoverflow.com/q/598defaultPointerReduction1642
	// https://prnt.sc/kmkTmVmO2cPH
	// Pointer connected method and non pointer connect methods will be different
	structType := rv.Type()
	methodsCount := rv.NumMethod()
	methodsMap := make(
		map[string]*reflect.Method,
		methodsCount)

	for i := 0; i < methodsCount; i++ {
		method := structType.Method(i)
		methodsMap[method.Name] = &method
	}

	return methodsMap
}
