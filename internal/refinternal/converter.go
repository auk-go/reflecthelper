package refinternal

import (
	"reflect"
	"unsafe"

	"gitlab.com/auk-go/reflecthelper/reflectmodel"
)

type converter struct{}

func (it *converter) InterfacesToTypes(items []interface{}) []reflect.Type {
	if len(items) == 0 {
		return []reflect.Type{}
	}

	var output []reflect.Type

	for _, item := range items {
		toType := reflect.TypeOf(item)
		output = append(output, toType)

	}

	return output
}

func (it *converter) InterfacesToTypesNames(items []interface{}) []string {
	if len(items) == 0 {
		return []string{}
	}

	var output []string

	for _, item := range items {
		toType := reflect.TypeOf(item)
		output = append(output, toType.Name())

	}

	return output
}

func (it *converter) ReflectValueToPointerReflectValue(
	rv reflect.Value,
) reflect.Value {
	toInterface := rv.Interface()
	toPointer := &toInterface
	unsafePtr := unsafe.Pointer(&toPointer)

	return reflect.NewAt(rv.Type(), unsafePtr)
}

// ReducePointer
//
// anyItem must be a struct or pointer to struct
//
// level means how many ****Struct to reduce to Struct
func (it *converter) ReducePointer(
	anyItem interface{},
	level int,
) *reflectmodel.ReflectValueKind {
	rv := reflect.ValueOf(anyItem) // can be a pointer or non pointer

	return it.ReducePointerRv(rv, level)
}

// ReducePointerRv
//
// # Rv must be a struct or pointer to struct
//
// level means how many ****Struct to reduce to Struct
func (it *converter) ReducePointerRv(
	rv reflect.Value,
	level int,
) *reflectmodel.ReflectValueKind {
	return Looper.ReducePointerRv(rv, level)
}

// ReducePointerDefault
//
// anyItem must be a struct or pointer to struct
//
// Default means level 3 at max
func (it *converter) ReducePointerDefault(
	anyItem interface{},
) *reflectmodel.ReflectValueKind {
	return it.ReducePointerDefault(anyItem)
}

// ReducePointerRvDefault
//
// # Rv must be a struct or pointer to struct
//
// level means how many ****Struct to reduce to Struct
//
// Default means level 3
func (it *converter) ReducePointerRvDefault(
	rv reflect.Value,
) *reflectmodel.ReflectValueKind {
	return Looper.ReducePointerRvDefault(rv)
}

func (it *converter) ReducePointerDefaultToType(
	anyItem interface{},
) *reflect.Type {
	rv := reflect.ValueOf(anyItem)

	return it.ReducePointerRvDefaultToType(rv)
}

// ReducePointerRvDefaultToType
//
// # Rv must be a struct or pointer to struct
//
// level means how many ****Struct to reduce to Struct
//
// Default means level 3
func (it *converter) ReducePointerRvDefaultToType(
	rv reflect.Value,
) *reflect.Type {
	result := Looper.ReducePointerRvDefault(rv)

	if result != nil {
		toType := result.FinalReflectVal.Type()

		return &toType
	}

	return nil
}
