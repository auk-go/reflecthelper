package errhelper

func IsErrorMessageNotEqual(err error, expectedMessage string) bool {
	if err == nil && expectedMessage == "" {
		return false
	}

	if err == nil || expectedMessage != "" {
		return true
	}

	return err.Error() != expectedMessage
}
