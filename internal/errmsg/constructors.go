package errmsg

import (
	"fmt"
	"reflect"
)

// New constructs new error from the given message and arguments
func New(msg string, values ...interface{}) error {
	return fmt.Errorf(msg, values...)
}

func ExpectedButFound(expected, found reflect.Kind) error {
	return New("expected [%v] but found [%v]", expected.String(), found.String())
}

func ExpectedButFoundString(expected, found string) error {
	return New("expected [%v] but found [%v]", expected, found)
}

func ExpectedPrimitiveButFound(found reflect.Kind) error {
	return New("expected [primitive] but found [%v]", found.String())
}

func GenericValueIsNilOrHasNoElement(typ reflect.Kind) error {
	return New("generic value [%v] is nil or has no element", typ.String())
}
