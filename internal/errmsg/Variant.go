package errmsg

import "fmt"

type Variant string

const (
	FieldIsNotPresentIn              Variant = "field is not present in %q"
	FieldIsNotExportedIn             Variant = "field is not exported in %q"
	FieldIsNilIn                     Variant = "field is nil in %q"
	FieldTypeMismatch                Variant = "field is of type %q in %q but %q in %q"
	InvalidModel                     Variant = "model is invalid"
	FieldIsEmpty                     Variant = "field is empty"
	CanNotConvertFromToMsgFormat     Variant = "can not convert from %s to %s"
	FieldNameIsNotPresentInMsgFormat Variant = "field [%s] is not present in %s"
)

func (it Variant) String() string {
	return (string)(it)
}

func (it Variant) Value() string {
	return (string)(it)
}

func (it Variant) Error(params ...interface{}) error {
	return fmt.Errorf(it.Value(), params...)
}

func (it Variant) ErrorNoArgs() error {
	return fmt.Errorf(it.Value())
}
