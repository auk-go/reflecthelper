package isreflect

import "reflect"

func DefinedAny(anyItem interface{}) bool {
	return !Null(reflect.ValueOf(anyItem))
}
