package isreflect

import "reflect"

func TypeOfAny(anyItem interface{}, kinds ...reflect.Kind) bool {
	if len(kinds) == 0 {
		return false
	}

	rv := reflect.ValueOf(anyItem)
	currentKind := rv.Kind()

	for _, kind := range kinds {
		if kind == currentKind {
			return true
		}
	}

	return false
}
