package isreflect

import "reflect"

func NullAny(anyItem interface{}) bool {
	return Null(reflect.ValueOf(anyItem))
}
