package isreflect

import "reflect"

func ReflectPointer(rv reflect.Value) bool {
	return rv.Kind() == reflect.Ptr
}
