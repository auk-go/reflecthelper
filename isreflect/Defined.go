package isreflect

import "reflect"

func Defined(rv reflect.Value) bool {
	return !Null(rv)
}
