package isreflect

import "reflect"

func FloatingPoint(anyItem interface{}) bool {
	return ReflectFloatingPoint(reflect.ValueOf(anyItem))
}
