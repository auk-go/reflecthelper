package isreflect

import "reflect"

func ReflectFunction(rv reflect.Value) bool {
	return rv.Kind() == reflect.Func
}
