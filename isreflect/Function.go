package isreflect

import "reflect"

func Function(anyItem interface{}) bool {
	rv := reflect.ValueOf(anyItem)
	currentKind := rv.Kind()

	return currentKind == reflect.Func
}
