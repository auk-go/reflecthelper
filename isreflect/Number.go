package isreflect

import "reflect"

func Number(anyItem interface{}) bool {
	return ReflectNumber(reflect.ValueOf(anyItem))
}
