package isreflect

import "reflect"

func Pointer(anyItem interface{}) bool {
	rv := reflect.ValueOf(anyItem)
	currentKind := rv.Kind()

	return currentKind == reflect.Ptr
}
