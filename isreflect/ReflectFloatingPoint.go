package isreflect

import "reflect"

func ReflectFloatingPoint(rv reflect.Value) bool {
	switch rv.Kind() {
	case reflect.Float32, reflect.Float64:
		return true
	default:
		return false
	}
}
