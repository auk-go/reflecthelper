package isreflect

import "reflect"

func PositiveInteger(anyItem interface{}) bool {
	return ReflectPositiveInteger(reflect.ValueOf(anyItem))
}
