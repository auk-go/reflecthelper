package reflecthelper

import (
	"reflect"

	"gitlab.com/auk-go/reflecthelper/internal/refinternal"
	"gitlab.com/auk-go/reflecthelper/reflectmodel"
)

type looper struct{}

// ReducePointer
// level -1 means all levels (****...) to Non pointer
func (it *looper) ReducePointer(
	anyItem interface{},
	level int,
) *reflectmodel.ReflectValueKind {
	return refinternal.
		Looper.
		ReducePointer(
			reflect.ValueOf(anyItem),
			level)
}

func (it *looper) ReducePointerDefault(
	anyItem interface{},
) *reflectmodel.ReflectValueKind {
	return refinternal.
		Looper.
		ReducePointerDefault(anyItem)
}

// ReducePointerRv
//
//	level -1 means all levels (****...) to Non pointer
func (it *looper) ReducePointerRv(
	reflectVal reflect.Value,
	level int,
) *reflectmodel.ReflectValueKind {
	return refinternal.
		Looper.
		ReducePointerRv(
			reflectVal,
			level)
}

func (it *looper) ReducePointerRvDefault(
	reflectVal reflect.Value,
) *reflectmodel.ReflectValueKind {
	return refinternal.
		Looper.
		ReducePointerRvDefault(reflectVal)
}

func (it *looper) FieldsFor(
	anyItem interface{},
	fieldProcessorFunc FieldsProcessorFunc,
) error {
	return refinternal.Looper.FieldsFor(
		anyItem,
		fieldProcessorFunc)
}

func (it *looper) MethodsFor(
	anyItem interface{},
	methodProcessorFunc MethodProcessorFunc,
) error {
	return refinternal.Looper.MethodsFor(
		anyItem,
		methodProcessorFunc)
}

func (it *looper) FieldsForRv(
	rv reflect.Value,
	fieldProcessorFunc FieldsProcessorFunc,
) error {
	return refinternal.Looper.FieldsForRv(
		rv,
		fieldProcessorFunc)
}

func (it *looper) MethodsForRv(
	rv reflect.Value,
	methodProcessorFunc MethodProcessorFunc,
) error {
	return refinternal.Looper.MethodsForRv(
		rv,
		methodProcessorFunc)
}

func (it *looper) FieldNames(
	anyStruct interface{},
) (fieldNames []string, err error) {
	rv := reflect.ValueOf(anyStruct)

	return it.FieldNamesRv(rv)
}

func (it *looper) FieldNamesRv(
	rv reflect.Value,
) (fieldNames []string, err error) {
	return refinternal.Looper.FieldNamesRv(
		rv)
}

func (it *looper) FieldsMap(
	anyItem interface{},
) (resultsMap map[string]*reflect.StructField, err error) {
	rv := reflect.ValueOf(anyItem)

	return it.FieldsMapRv(rv)
}

func (it *looper) FieldsMapRv(
	rv reflect.Value,
) (resultsMap map[string]*reflect.StructField, err error) {
	return refinternal.Looper.FieldsMapRv(
		rv)
}

func (it *looper) MethodsMap(
	anyItem interface{},
) (resultsMap map[string]*reflect.Method, err error) {
	rv := reflect.ValueOf(anyItem)

	return it.MethodsMapRv(rv)
}

func (it *looper) MethodNamesRv(
	rv reflect.Value,
) (methodNames []string, err error) {
	return refinternal.Looper.MethodNamesRv(
		rv)
}

func (it *looper) MethodsMapRv(
	rv reflect.Value,
) (map[string]*reflect.Method, error) {
	return refinternal.Looper.MethodsMapRv(
		rv)
}

// ToPointerReflectValue
//
// anyItem must be a struct or pointer to struct
func (it *looper) ToPointerReflectValue(
	anyItem interface{},
) (reflect.Value, error) {
	// valid
	// https://stackoverflow.com/q/598defaultPointerReduction1642
	// https://prnt.sc/kmkTmVmO2cPH
	// Pointer connected method and non pointer connect methods will be different
	rv := reflect.ValueOf(anyItem) // can be a pointer or non pointer

	return it.ToPointerReflectValueRv(rv)
}

// ToPointerReflectValueRv
//
// Rv must be a struct or pointer to struct
func (it *looper) ToPointerReflectValueRv(
	rv reflect.Value,
) (reflect.Value, error) {
	return refinternal.Looper.ToPointerReflectValueRv(
		rv)
}
