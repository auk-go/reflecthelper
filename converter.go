package reflecthelper

import (
	"reflect"

	"gitlab.com/auk-go/reflecthelper/internal/refinternal"
	"gitlab.com/auk-go/reflecthelper/reflectmodel"
)

type converter struct{}

func (it *converter) InterfacesToTypes(items []interface{}) []reflect.Type {
	return refinternal.Converter.InterfacesToTypes(items)
}

func (it *converter) InterfacesToTypesNames(items []interface{}) []string {
	return refinternal.Converter.InterfacesToTypesNames(items)
}

func (it *converter) ReflectValueToPointerReflectValue(
	rv reflect.Value,
) reflect.Value {
	return refinternal.Converter.ReflectValueToPointerReflectValue(rv)
}

// ReducePointer
//
// anyItem must be a struct or pointer to struct
//
// level means how many ****Struct to reduce to Struct
func (it *converter) ReducePointer(
	anyItem interface{},
	level int,
) *reflectmodel.ReflectValueKind {
	return refinternal.Converter.ReducePointer(anyItem, level)
}

// ReducePointerRv
//
// # Rv must be a struct or pointer to struct
//
// level means how many ****Struct to reduce to Struct
func (it *converter) ReducePointerRv(
	rv reflect.Value,
	level int,
) *reflectmodel.ReflectValueKind {
	return refinternal.Converter.ReducePointer(rv, level)
}

// ReducePointerDefault
//
// anyItem must be a struct or pointer to struct
//
// Default means level 3 at max
func (it *converter) ReducePointerDefault(
	anyItem interface{},
) *reflectmodel.ReflectValueKind {
	return it.ReducePointerDefault(anyItem)
}

// ReducePointerRvDefault
//
// # Rv must be a struct or pointer to struct
//
// level means how many ****Struct to reduce to Struct
//
// Default means level 3
func (it *converter) ReducePointerRvDefault(
	rv reflect.Value,
) *reflectmodel.ReflectValueKind {
	return Looper.ReducePointerRvDefault(rv)
}

func (it *converter) ReducePointerDefaultToType(
	anyItem interface{},
) *reflect.Type {
	rv := reflect.ValueOf(anyItem)

	return it.ReducePointerRvDefaultToType(rv)
}

// ReducePointerRvDefaultToType
//
// # Rv must be a struct or pointer to struct
//
// level means how many ****Struct to reduce to Struct
//
// Default means level 3
func (it *converter) ReducePointerRvDefaultToType(
	rv reflect.Value,
) *reflect.Type {
	return refinternal.Converter.ReducePointerRvDefaultToType(rv)
}
