package reflecthelper

// SetFromTo gets the data in to out by using reflection
//
// Example
//
//	TODO example
func SetFromTo(opt *Options, from interface{}, to interface{}) error {
	g := newGetter(opt, false)

	return g.SetFromTo(from, to)
}
