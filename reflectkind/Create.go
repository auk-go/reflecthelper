package reflectkind

import "reflect"

func Create(anyItem interface{}) Enhance {
	kind := reflect.ValueOf(anyItem).Kind()

	return Enhance(kind)
}

func CreateUsingRv(rv reflect.Value) Enhance {
	kind := rv.Kind()

	return Enhance(kind)
}

func CreateUsingType(rType reflect.Type) Enhance {
	kind := rType.Kind()

	return Enhance(kind)
}

func CreateUsingField(field reflect.StructField) Enhance {
	kind := field.Type.Kind()

	return Enhance(kind)
}
