package reflectfields

import (
	"reflect"

	"gitlab.com/auk-go/reflecthelper/internal/consts"
	"gitlab.com/auk-go/reflecthelper/reflectwrapper"
)

// GetNullFieldsUsingReflectVal
//
// if not valid then returns empty
//
// Includes both public privates.
func GetNullFieldsUsingReflectVal(
	reflectVal reflect.Value,
	level int,
) *reflectwrapper.SimpleHashset {
	structType := reflectVal.Type()
	structValueKind := reflectVal.Kind()
	hasLevel := level > consts.Invalid
	structValue := reflectVal

	// reducing ****ToValue to ToValue
	for structValueKind == reflect.Ptr || structValueKind == reflect.Interface {
		// mutating dangerous code
		structValue = structValue.Elem()
		structValueKind = structValue.Kind()

		level--
		if hasLevel && level <= 0 {
			break
		}
	}

	if !structValue.IsValid() || structValueKind != reflect.Struct {
		return reflectwrapper.Empty.SimpleHashset()
	}

	structNumFields := structType.NumField()
	hashset := reflectwrapper.New.SimpleHashset.Cap(structNumFields)
	var fieldValue reflect.Value
	var fieldType reflect.StructField

	for i := 0; i < structNumFields; i++ {
		fieldValue = structValue.Field(i)

		if fieldValue.IsNil() {
			fieldType = structType.Field(i)
			hashset.Add(fieldType.Name)
		}
	}

	return hashset
}
