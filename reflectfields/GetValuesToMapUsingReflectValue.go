package reflectfields

import (
	"reflect"
	"unsafe"
)

// GetValuesToMapUsingReflectValue unlike GetPublicValuesToMap to map it collects
// all fields with values including the private ones.
//
// However, this one will be slower in performance than GetPublicValuesToMap.
func GetValuesToMapUsingReflectValue(structValue reflect.Value) (
	map[string]interface{}, error,
) {
	structType := structValue.Type()
	structNumFields := structType.NumField()
	fieldToValueMap := make(map[string]interface{}, structNumFields)

	// structValue is not addressable, create a temporary copy
	if !structValue.CanAddr() {
		newType := reflect.New(structType).Elem()
		newType.Set(structValue)
		// structValue is now addressable
		structValue = newType
	}

	for i := 0; i < structNumFields; i++ {
		fieldType := structType.Field(i)
		fieldValue := structValue.Field(i)

		if fieldType.PkgPath != "" {
			unexportedField := reflect.NewAt(
				fieldType.Type,
				unsafe.Pointer(fieldValue.UnsafeAddr())).Elem()
			fieldToValueMap[fieldType.Name] = unexportedField.Interface()
		} else {
			fieldToValueMap[fieldType.Name] = fieldValue.Interface()
		}
	}

	return fieldToValueMap, nil
}
