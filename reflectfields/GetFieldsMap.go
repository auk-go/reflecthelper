package reflectfields

import "reflect"

func GetFieldsMap(input interface{}) map[string]reflect.StructField {
	structValue := reflect.ValueOf(input)

	return GetFieldsMapUsingReflect(structValue)
}
