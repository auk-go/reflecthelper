package reflectfields

import (
	"reflect"

	"gitlab.com/auk-go/reflecthelper/reflectwrapper"
)

// GetNullFields
//
// Includes both public privates.
func GetNullFields(
	any interface{},
	level int,
) *reflectwrapper.SimpleHashset {
	rv := reflect.ValueOf(any)

	return GetNullFieldsUsingReflectVal(rv, level)
}
