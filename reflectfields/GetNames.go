package reflectfields

import (
	"reflect"

	"gitlab.com/auk-go/reflecthelper/reflectwrapper"
)

// GetNames returns a set of all struct fields
//
// input can be ptr, interface and will be
// reduced to struct and returns the hashset of field names for it
func GetNames(input interface{}) (*reflectwrapper.SimpleHashset, error) {
	structValue := reflect.ValueOf(input)

	return GetNamesUsingReflectVal(structValue)
}
