package reflectfields

import (
	"reflect"

	"gitlab.com/auk-go/reflecthelper/internal/errmsg"
	"gitlab.com/auk-go/reflecthelper/reflectwrapper"
)

// GetNamesUsingReflectVal returns a set of all struct fields
//
// input can be ptr, interface and will be
// reduced to struct and returns the hashset of field names for it
func GetNamesUsingReflectVal(rv reflect.Value) (*reflectwrapper.SimpleHashset, error) {
	structValue := rv
	structValueKind := structValue.Kind()

	for structValueKind == reflect.Ptr || structValueKind == reflect.Interface {
		// mutating dangerous code
		structValue = structValue.Elem()
		structValueKind = structValue.Kind()
	}

	if !structValue.IsValid() || structValueKind != reflect.Struct {
		return reflectwrapper.Empty.SimpleHashset(),
			errmsg.ExpectedButFound(reflect.Struct, structValueKind)
	}

	structType := structValue.Type()
	fieldsLength := structType.NumField()
	fieldsHashset :=
		reflectwrapper.New.SimpleHashset.Cap(fieldsLength)
	var name string

	for i := 0; i < fieldsLength; i++ {
		name = structType.Field(i).Name
		fieldsHashset.Add(name)
	}

	return fieldsHashset, nil
}
