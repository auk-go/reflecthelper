package reflectreq

import (
	"fmt"
	"reflect"

	"gitlab.com/auk-go/reflecthelper/internal/consts"
)

type MismatchTagStruct struct {
	FieldQueryName string // "Field1.Field2.Field3"
	Expected       string
	Actual         reflect.StructTag
}

func (it MismatchTagStruct) String() string {
	return fmt.Sprintf(
		consts.QueryFieldNameToActualExpectedLogFormat,
		it.FieldQueryName,
		it.Actual,
		it.Expected)
}
