package reflectreq

import (
	"reflect"
)

type ChildFieldTypesExpectationRequest struct {
	RawInputStruct            interface{}
	FieldsNameQueryRequestMap map[string]reflect.Type // Key =>"Field1.Field2.Field3" (Field name query), Val => Expected type
}

func (it *ChildFieldTypesExpectationRequest) Length() int {
	if it == nil {
		return 0
	}

	return len(it.FieldsNameQueryRequestMap)
}

func (it *ChildFieldTypesExpectationRequest) IsEmpty() bool {
	return it.Length() == 0
}

func (it *ChildFieldTypesExpectationRequest) FieldNameQueries() []string {
	if it == nil || len(it.FieldsNameQueryRequestMap) == 0 {
		return nil
	}

	list := make(
		[]string,
		0,
		it.Length())

	for fieldQueryKey := range it.FieldsNameQueryRequestMap {
		list = append(list, fieldQueryKey)
	}

	return list
}
