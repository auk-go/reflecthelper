package reflectreq

import "gitlab.com/auk-go/reflecthelper/reflectwrapper"

var (
	defaultPublicValueGetter = func(wrapper *reflectwrapper.FieldWrapper) (val interface{}) {
		if wrapper == nil || !wrapper.IsPublic {
			return nil
		}

		return wrapper.PublicValue()
	}

	defaultAllValueGetter = func(wrapper *reflectwrapper.FieldWrapper) (val interface{}) {
		if wrapper == nil {
			return nil
		}

		return wrapper.ValueInterfaceOrError()
	}
)
