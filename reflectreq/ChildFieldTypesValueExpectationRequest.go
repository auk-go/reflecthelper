package reflectreq

import "reflect"

type ChildFieldTypesValueExpectationRequest struct {
	RawInputStruct            interface{}
	FieldsNameQueryRequestMap map[string]interface{} // Key =>"F1.F2.F3", Val => Expected Value => type
}

func (it *ChildFieldTypesValueExpectationRequest) Length() int {
	if it == nil {
		return 0
	}

	return len(it.FieldsNameQueryRequestMap)
}

func (it *ChildFieldTypesValueExpectationRequest) IsEmpty() bool {
	return it.Length() == 0
}

func (it *ChildFieldTypesValueExpectationRequest) ChildFieldTypesExpectationRequest() *ChildFieldTypesExpectationRequest {
	if it == nil {
		return nil
	}

	fieldsNameQueryRequestMap := make(
		map[string]reflect.Type,
		it.Length())

	for key, value := range it.FieldsNameQueryRequestMap {
		fieldsNameQueryRequestMap[key] = reflect.TypeOf(value)
	}

	return &ChildFieldTypesExpectationRequest{
		RawInputStruct:            it.RawInputStruct,
		FieldsNameQueryRequestMap: fieldsNameQueryRequestMap,
	}
}
