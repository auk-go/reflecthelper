package reflectreq

import (
	"fmt"
	"reflect"

	"gitlab.com/auk-go/reflecthelper/internal/consts"
)

type MismatchType struct {
	FieldQueryName   string // F1.F2.F3
	Actual, Expected reflect.Type
}

func (it MismatchType) String() string {
	return fmt.Sprintf(
		consts.QueryFieldNameToActualExpectedLogFormat,
		it.FieldQueryName,
		it.Actual,
		it.Expected)
}
