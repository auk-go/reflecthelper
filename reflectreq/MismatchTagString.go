package reflectreq

import (
	"fmt"

	"gitlab.com/auk-go/reflecthelper/internal/consts"
)

type MismatchTagString struct {
	FieldQueryName   string // "Field1.Field2.Field3"
	Actual, Expected string
}

func (it MismatchTagString) String() string {
	return fmt.Sprintf(
		consts.QueryFieldNameToActualExpectedLogFormat,
		it.FieldQueryName,
		it.Actual,
		it.Expected)
}
