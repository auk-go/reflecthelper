package reflectwrapper

import (
	"reflect"

	"gitlab.com/auk-go/reflecthelper/reflectkind"
)

type newValueWrapperCreator struct{}

func (it *newValueWrapperCreator) Invalid() *ValueWrapper {
	return &ValueWrapper{}
}

func (it *newValueWrapperCreator) Create(
	anyItem interface{},
) *ValueWrapper {
	rv := reflect.ValueOf(anyItem)
	kind := rv.Kind()

	valueWrapper := ValueWrapper{
		rawItem:     anyItem,
		rv:          rv,
		kind:        kind,
		enhanceKind: reflectkind.Enhance(kind),
	}

	return &valueWrapper
}

func (it *newValueWrapperCreator) CreateRv(
	rv reflect.Value,
) *ValueWrapper {
	kind := rv.Kind()

	valueWrapper := ValueWrapper{
		rawItem:     rv.Interface(),
		rv:          rv,
		kind:        kind,
		enhanceKind: reflectkind.Enhance(kind),
	}

	return &valueWrapper
}
