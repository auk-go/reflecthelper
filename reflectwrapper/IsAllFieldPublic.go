package reflectwrapper

import "reflect"

func IsAllFieldPublic(fields ...reflect.StructField) bool {
	if len(fields) == 0 {
		return false
	}

	for _, field := range fields {
		if field.PkgPath != "" {
			return false
		}
	}

	return true
}

func IsAllMapFieldPublic(fieldMap map[string]reflect.StructField) bool {
	if len(fieldMap) == 0 {
		return false
	}

	for _, field := range fieldMap {
		if field.PkgPath != "" {
			return false
		}
	}

	return true
}
