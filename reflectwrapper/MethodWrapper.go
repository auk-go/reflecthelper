package reflectwrapper

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/auk-go/reflecthelper/internal/refinternal"
)

// MethodWrapper
//
// A wrapper for a method, usually get created from a struct
// It can be also created by reflectwrapper.New.MethodWrapper.Create(...)
type MethodWrapper struct {
	MethodReflectValue reflect.Value
	Method             reflect.Method
	Name               string
	Index              int
	IsValid            bool
	inArgsTypesNames   []string
	inArgsTypes        []reflect.Type
	outArgsTypes       []reflect.Type
}

func (it *MethodWrapper) IsInvalid() bool {
	return it == nil || !it.IsValid
}

func (it *MethodWrapper) InvokeMethodNoArgs(methodName string) []reflect.Value {
	if it.IsInvalid() {
		panic("method not found!")
	}

	return it.
		MethodReflectValue.
		MethodByName(methodName).
		Call([]reflect.Value{})
}

// ArgsCount returns -1 on invalid
func (it *MethodWrapper) ArgsCount() int {
	if it.IsInvalid() {
		return -1
	}

	// https://stackoverflow.com/a/47626214

	return it.Method.Type.NumIn()
}

// ArgsLength is an Alias for ArgsCount
func (it *MethodWrapper) ArgsLength() int {
	return it.ArgsCount()
}

// ReturnLength refers to the return arguments length
func (it *MethodWrapper) ReturnLength() int {
	if it.IsInvalid() {
		return -1
	}

	// https://stackoverflow.com/a/47626214

	return it.Method.Type.NumOut()
}

func (it *MethodWrapper) IsPublicMethod() bool {
	return it != nil && it.Method.PkgPath == ""
}

func (it *MethodWrapper) IsPrivateMethod() bool {
	return it != nil && it.Method.PkgPath != ""
}

func (it *MethodWrapper) GetType() reflect.Type {
	if it.IsInvalid() {
		return nil
	}

	return it.MethodReflectValue.Type()
}

func (it *MethodWrapper) GetOutArgsTypes() []reflect.Type {
	if it.IsInvalid() {
		return []reflect.Type{}
	}

	argsOutCount := it.ReturnLength()

	if argsOutCount == 0 {
		return []reflect.Type{}
	}

	if len(it.outArgsTypes) == argsOutCount {
		return it.outArgsTypes
	}

	// https://go.dev/play/p/dpIspUFfbu0
	mainType := it.MethodReflectValue.Type()
	slice := make([]reflect.Type, 0, argsOutCount)

	for i := 0; i < argsOutCount; i++ {
		slice = append(slice, mainType.Out(i))
	}

	it.outArgsTypes = slice

	return slice
}

func (it *MethodWrapper) GetInArgsTypes() []reflect.Type {
	if it.IsInvalid() {
		return []reflect.Type{}
	}

	argsCount := it.ArgsCount()

	if argsCount == 0 {
		return []reflect.Type{}
	}

	if len(it.inArgsTypes) == argsCount {
		return it.inArgsTypes
	}

	// https://go.dev/play/p/dpIspUFfbu0
	mainType := it.MethodReflectValue.Type()
	slice := make([]reflect.Type, 0, argsCount)

	for i := 0; i < argsCount; i++ {
		slice = append(slice, mainType.In(i))
	}

	it.inArgsTypes = slice

	return slice
}

func (it *MethodWrapper) GetInArgsTypesNames() []string {
	if it.IsInvalid() {
		return []string{}
	}

	argsCount := it.ArgsCount()

	if argsCount == 0 {
		return []string{}
	}

	if len(it.inArgsTypesNames) == argsCount {
		return it.inArgsTypesNames
	}

	// https://go.dev/play/p/dpIspUFfbu0
	mainType := it.MethodReflectValue.Type()
	slice := make([]string, 0, argsCount)

	for i := 0; i < argsCount; i++ {
		slice = append(slice, mainType.In(i).Name())
	}

	it.inArgsTypesNames = slice

	return slice
}

func (it *MethodWrapper) VerifyInArgs(args []interface{}) (isOkay bool, err error) {
	toTypes := refinternal.Converter.InterfacesToTypes(args)

	return it.InArgsVerifyRv(toTypes)
}

func (it *MethodWrapper) VerifyOutArgs(args []interface{}) (isOkay bool, err error) {
	toTypes := refinternal.Converter.InterfacesToTypes(args)

	return it.OutArgsVerifyRv(toTypes)
}

func (it *MethodWrapper) InArgsVerifyRv(args []reflect.Type) (isOkay bool, err error) {
	return refinternal.Utils.VerifyReflectTypes(it.GetInArgsTypes(), args)
}

func (it *MethodWrapper) OutArgsVerifyRv(args []reflect.Type) (isOkay bool, err error) {
	return refinternal.Utils.VerifyReflectTypes(it.GetOutArgsTypes(), args)
}

func (it *MethodWrapper) InvokeMethodDirectly(
	args ...interface{},
) (returnedValues []reflect.Value, err error) {
	it.mustBeValid()

	argsReflectValues := ArgsReflectValues(
		args)

	values := it.MethodReflectValue.Call(argsReflectValues)

	return values, nil
}

func (it *MethodWrapper) InvokeMethodDirectlyVoid(
	args ...interface{},
) error {
	it.mustBeValid()

	it.Invoke(args)

	return nil
}

func (it *MethodWrapper) mustBeValid() {
	if it == nil {
		panic("cannot execute on nil method-wrapper")
	}

	if it.IsInvalid() {
		panic("method invalid - " + it.Name)
	}
}

func (it *MethodWrapper) InvokeVoidMethod(
	args ...interface{},
) {
	it.mustBeValid()

	argsReflectValues := ArgsReflectValues(args)
	it.MethodReflectValue.
		Call(argsReflectValues)
}

func (it *MethodWrapper) InvokeMethod(
	args ...interface{},
) []reflect.Value {
	it.mustBeValid()
	it.ValidateMethodArgs(args)

	argsReflectValues := ArgsReflectValues(args)

	return it.
		Method.
		Func.
		Call(argsReflectValues)
}

func (it *MethodWrapper) Invoke(
	args ...interface{},
) []interface{} {
	it.ValidateMethodArgs(args)

	returnedValues := it.InvokeMethod(
		args...,
	)

	return ReflectValuesToInterfaces(
		returnedValues)
}

func (it *MethodWrapper) ValidateMethodArgs(args []interface{}) {
	expectedCount := it.ArgsCount()
	given := len(args)

	if given != expectedCount {
		panic(it.argsCountMismatchErrorMessage(expectedCount, given, args))
	}

	_, err := it.VerifyInArgs(args)

	if err != nil {
		panic(err)
	}
}

func (it *MethodWrapper) argsCountMismatchErrorMessage(expectedCount int, given int, args []interface{}) string {
	expectedTypes := it.GetInArgsTypesNames()
	expectedToNames := strings.Join(expectedTypes, "\n\t -")
	actualTypes := refinternal.Converter.InterfacesToTypesNames(args)
	actualTypesName := strings.Join(actualTypes, "\n\t -")

	return fmt.Sprintf(
		"%s [Method] -> "+
			"arguments count doesn't match for - Count - expected : "+
			"%d, given : %d\nexpected types listed : %s\nactual given types list : %s",
		it.Name,
		expectedCount,
		given,
		expectedToNames,
		actualTypesName)
}

func (it *MethodWrapper) GetFirstResponseOfInvoke(
	args ...interface{},
) (
	firstResponse interface{},
) {
	return it.GetResponseOfIndexFromInvoke(0, args...)
}

func (it *MethodWrapper) GetResponseOfIndexFromInvoke(
	index int,
	args ...interface{},
) (
	firstResponse interface{},
) {
	results := it.Invoke(args...)

	return results[index]
}

func (it *MethodWrapper) InvokeError(
	args ...interface{},
) (err error) {
	return it.GetFirstResponseOfInvoke(args...).(error)
}

// InvokeFirstAndError
//
//	useful for method which looks like ReflectMethod() (soemthing, error)
func (it *MethodWrapper) InvokeFirstAndError(
	args ...interface{},
) (
	firstResponse interface{}, err error,
) {
	responses := it.Invoke(args...)

	first := responses[0]
	second := responses[1].(error)

	return first, second
}

// IsNotEqual
//
// Based on predication.
//
// Warning: it can be wrong as well
func (it *MethodWrapper) IsNotEqual(
	another *MethodWrapper,
) bool {
	return !it.IsEqual(another)
}

// IsEqual
//
// Based on predication.
//
// Warning: it can be wrong as well
func (it *MethodWrapper) IsEqual(
	another *MethodWrapper,
) bool {
	if it == nil && another == nil {
		return true
	}

	if it == nil || another == nil {
		return false
	}

	if it == another {
		return true
	}

	if it.IsInvalid() != another.IsInvalid() {
		return false
	}

	if it.Name != it.Name {
		return false
	}

	// can be skipped,
	// because name also refers to public or private
	if it.IsPublicMethod() != it.IsPublicMethod() {
		return false
	}

	if it.ArgsCount() != it.ArgsCount() {
		return false
	}

	if it.ReturnLength() != it.ReturnLength() {
		return false
	}

	isInArgsOkay, _ := it.InArgsVerifyRv(another.GetInArgsTypes())

	if !isInArgsOkay {
		return false
	}

	isOutArgsOkay, _ := it.OutArgsVerifyRv(another.GetOutArgsTypes())

	if !isOutArgsOkay {
		return false
	}

	// most probably true,
	// but can be false as well

	return true
}
