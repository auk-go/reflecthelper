package reflectwrapper

import "reflect"

func GetRawFieldValue(anyStruct interface{}, fieldName string) reflect.Value {
	rVal := reflect.ValueOf(anyStruct)

	return rVal.FieldByName(fieldName)
}
