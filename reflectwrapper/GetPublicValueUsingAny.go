package reflectwrapper

import (
	"reflect"
)

func GetPublicValueUsingAny(anyStruct interface{}, fieldName string) interface{} {
	rVal := reflect.ValueOf(anyStruct)
	rField := rVal.FieldByName(fieldName)

	return GetPublicValue(rField)
}
