package reflectwrapper

import "reflect"

func InterfacesToReflectValues(
	anyItems ...interface{},
) []reflect.Value {
	if len(anyItems) == 0 {
		return []reflect.Value{}
	}

	list := make(
		[]reflect.Value,
		len(anyItems))

	for i, rv := range anyItems {
		list[i] = reflect.ValueOf(rv)
	}

	return list
}
