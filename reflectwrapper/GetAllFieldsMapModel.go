package reflectwrapper

import (
	"gitlab.com/auk-go/reflecthelper/internal/refinternal"
	"gitlab.com/auk-go/reflecthelper/reflectmodel"
)

func GetAllFieldsMapModel(anyStruct interface{}) map[string]reflectmodel.FieldProcessor {
	itemsMap := make(map[string]reflectmodel.FieldProcessor, 5)
	err := refinternal.Looper.FieldsFor(anyStruct,
		func(currentField *reflectmodel.FieldProcessor) (err error) {
			itemsMap[currentField.Name] = *currentField

			return nil
		})

	if err != nil {
		panic(err)
	}

	return itemsMap
}
