package reflectwrapper

type (
	SimpleFilterFunc                     func(wrapper *FieldWrapper) (isTake bool)
	SimpleSkipFilterFunc                 func(wrapper *FieldWrapper) (isSkip bool)
	WrapperToStringProcessorFunc         func(index int, wrapper *FieldWrapper) string
	WrapperToValueInterfaceProcessorFunc func(index int, wrapper *FieldWrapper) interface{}
	FilterFunc                           func(
		index int,
		wrapper *FieldWrapper,
	) (isBreak, isTake bool)

	MethodFilterFunc func(
		m *MethodWrapper,
	) (isBreak, isTake bool)
)
