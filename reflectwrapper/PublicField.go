package reflectwrapper

import "reflect"

func PublicField(anyStruct interface{}, fieldName string) interface{} {
	rVal := reflect.ValueOf(anyStruct)

	return rVal.FieldByName(fieldName)
}
