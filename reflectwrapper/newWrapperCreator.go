package reflectwrapper

import (
	"reflect"

	"gitlab.com/auk-go/reflecthelper/internal/fieldname"
)

type newWrapperCreator struct{}

func (it *newWrapperCreator) Invalid() *FieldWrapper {
	return &FieldWrapper{}
}

func (it *newWrapperCreator) UsingAllParams(
	index int,
	fieldType *reflect.StructField,
	value reflect.Value,
) *FieldWrapper {
	kind := value.Kind()
	fieldName := fieldType.Name
	publicName := fieldname.TranspileToPublicMust(fieldName)
	privateName := fieldname.TranspileToPrivateMust(fieldName)

	wrapper := FieldWrapper{
		Field:              fieldType,
		Value:              value,
		Tag:                fieldType.Tag,
		Type:               fieldType.Type,
		Name:               fieldType.Name,
		PublicName:         publicName,
		PrivateName:        privateName,
		IsPublic:           fieldType.PkgPath == "",
		IsPointer:          kind == reflect.Ptr,
		IsValueAddressable: value.CanAddr(),
		HasSetter:          value.CanSet(),
		attributes:         nil,
		Kind:               kind,
		PkgPath:            fieldType.PkgPath,
		Index:              index,
		fieldsMap:          nil,
	}

	return &wrapper
}
