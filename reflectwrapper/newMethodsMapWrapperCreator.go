package reflectwrapper

import (
	"gitlab.com/auk-go/reflecthelper/internal/refinternal"
	"gitlab.com/auk-go/reflecthelper/reflectmodel"
)

type newMethodsMapWrappersCreator struct{}

func (it *newMethodsMapWrappersCreator) Invalid() *MethodsMapWrappers {
	return &MethodsMapWrappers{
		items:          map[string]*MethodWrapper{},
		orderedMethods: []string{},
	}
}

func (it *newMethodsMapWrappersCreator) empty(capacity int) *MethodsMapWrappers {
	return &MethodsMapWrappers{
		items:          make(map[string]*MethodWrapper, capacity),
		orderedMethods: make([]string, 0, capacity),
	}
}

func (it *newMethodsMapWrappersCreator) createNewUsing(
	inputMap map[string]*MethodWrapper,
	orderedMethods []string,
) *MethodsMapWrappers {
	return &MethodsMapWrappers{
		items:          inputMap,
		orderedMethods: orderedMethods,
	}
}

func (it *newMethodsMapWrappersCreator) Create(
	anyStruct interface{},
) *MethodsMapWrappers {
	newInstance := it.empty(5)
	err := refinternal.Looper.MethodsFor(
		anyStruct,
		func(
			totalMethodsCount int,
			method *reflectmodel.MethodProcessor,
		) (err error) {
			newInstance.addMethodModel(method)

			return nil
		})

	if err != nil {
		panic(err)
	}

	return newInstance
}

func (it *newMethodsMapWrappersCreator) CreateUsingWrappers(
	list []*MethodWrapper,
) *MethodsMapWrappers {
	newMap := make(map[string]*MethodWrapper, len(list))
	orderedNames := make([]string, 0, len(list))

	for _, wrapper := range list {
		methodName := wrapper.Name
		_, has := newMap[methodName]
		isMissing := !has

		if isMissing {
			orderedNames = append(orderedNames, wrapper.Name)
			newMap[wrapper.Name] = wrapper
		}
	}

	return it.createNewUsing(newMap, orderedNames)
}

func (it *newMethodsMapWrappersCreator) CreateExcept(
	anyStruct interface{},
	exceptMethods ...string,
) *MethodsMapWrappers {
	newInstance := it.empty(0)
	exceptNamesMap := map[string]bool{}
	if len(exceptMethods) > 0 {
		for _, methodName := range exceptMethods {
			exceptNamesMap[methodName] = true
		}
	}

	err := refinternal.Looper.MethodsFor(
		anyStruct,
		func(
			totalMethodsCount int,
			method *reflectmodel.MethodProcessor,
		) (err error) {
			if exceptNamesMap[method.Name] || method == nil {
				// skip it
				return nil
			}

			newInstance.addMethodModel(method)

			return nil
		})

	if err != nil {
		panic(err)
	}

	return newInstance
}

func (it *newMethodsMapWrappersCreator) SpecificMethods(
	anyStruct interface{},
	methodNames ...string,
) *MethodsMapWrappers {
	newInstance := it.empty(0)
	methodWrapperCreator := New.MethodWrapper.Create
	onlyNames := map[string]bool{}
	if len(methodNames) > 0 {
		for _, methodName := range methodNames {
			onlyNames[methodName] = true
		}
	}

	err := refinternal.Looper.MethodsFor(
		anyStruct,
		func(
			totalMethodsCount int,
			method *reflectmodel.MethodProcessor,
		) (err error) {
			if !onlyNames[method.Name] {
				// skip it
				return nil
			}

			methodWrapper := methodWrapperCreator(method.ReflectMethod)
			newInstance.add(methodWrapper)

			return nil
		})

	if err != nil {
		panic(err)
	}

	return newInstance
}

func (it *newMethodsMapWrappersCreator) CreateWithFilter(
	anyStruct interface{},
	isFilter func(methodWrapper *MethodWrapper) (isTake bool),
) *MethodsMapWrappers {
	newInstance := it.empty(0)
	methodWrapperCreator := New.MethodWrapper.Create
	err := refinternal.Looper.MethodsFor(
		anyStruct,
		func(
			totalMethodsCount int,
			method *reflectmodel.MethodProcessor,
		) (err error) {
			methodWrapper := methodWrapperCreator(method.ReflectMethod)

			if !isFilter(methodWrapper) {
				// skip it
				return nil
			}

			newInstance.add(methodWrapper)

			return nil
		})

	if err != nil {
		panic(err)
	}

	return newInstance
}
