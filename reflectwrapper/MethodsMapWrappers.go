package reflectwrapper

import (
	"encoding/json"
	"reflect"
	"strings"

	"gitlab.com/auk-go/reflecthelper/internal/refinternal"
	"gitlab.com/auk-go/reflecthelper/reflectmodel"
)

type MethodsMapWrappers struct {
	items          map[string]*MethodWrapper
	orderedMethods []string
	toString       string
}

func (it *MethodsMapWrappers) add(m *MethodWrapper) *MethodsMapWrappers {
	if it == nil {
		return nil
	}

	name := m.Name

	if it.HasMethod(name) {
		return it
	}

	it.items[name] = m
	it.orderedMethods = append(it.orderedMethods, name)

	return it
}

func (it *MethodsMapWrappers) addMethod(m reflect.Method) *MethodsMapWrappers {
	if it == nil {
		return nil
	}

	name := m.Name

	if it.HasMethod(name) {
		return it
	}

	it.items[name] = New.MethodWrapper.Create(m)
	it.orderedMethods = append(it.orderedMethods, name)

	return it
}

func (it *MethodsMapWrappers) addMethodModel(m *reflectmodel.MethodProcessor) *MethodsMapWrappers {
	if it == nil {
		return nil
	}

	name := m.Name

	if it.HasMethod(name) {
		return it
	}

	it.items[name] = New.MethodWrapper.Create(m.ReflectMethod)
	it.orderedMethods = append(it.orderedMethods, name)

	return it
}

func (it *MethodsMapWrappers) Length() int {
	if it == nil {
		return 0
	}

	return len(it.items)
}

func (it *MethodsMapWrappers) Count() int {
	return it.Length()
}

func (it *MethodsMapWrappers) IsEmpty() bool {
	return it.Length() == 0
}

func (it *MethodsMapWrappers) HasAnyItem() bool {
	return !it.IsEmpty()
}

func (it *MethodsMapWrappers) LastIndex() int {
	return it.Length() - 1
}

func (it *MethodsMapWrappers) HasIndex(index int) bool {
	return it.LastIndex() >= index
}

func (it *MethodsMapWrappers) Strings() []string {
	return it.orderedMethods
}

func (it *MethodsMapWrappers) HasMethod(name string) bool {
	_, has := it.items[name]

	return has
}

func (it *MethodsMapWrappers) GetMethod(name string) *MethodWrapper {
	methodWrapper, has := it.items[name]

	if has {
		return methodWrapper
	}

	return nil
}

func (it *MethodsMapWrappers) Get(name string) (*MethodWrapper, bool) {
	methodWrapper, has := it.items[name]

	return methodWrapper, has
}

func (it *MethodsMapWrappers) RawMap() map[string]*MethodWrapper {
	if it.IsEmpty() {
		return map[string]*MethodWrapper{}
	}

	return it.items
}

func (it *MethodsMapWrappers) String() string {
	if len(it.toString) > 0 {
		return it.toString
	}

	if it.Length() == 0 {
		return ""
	}

	it.toString = strings.Join(it.orderedMethods, ",")

	return it.toString
}

func (it *MethodsMapWrappers) Filter(
	filterFunc MethodFilterFunc,
) *MethodsMapWrappers {
	newInstance := New.MethodsMapWrappers.empty(it.Length() / 3)

	for _, name := range it.orderedMethods {
		methodWrapper := it.GetMethod(name)
		isTake, isBreak := filterFunc(methodWrapper)

		if isTake {
			newInstance.add(methodWrapper)
		}

		if isBreak {
			break
		}
	}

	return newInstance
}

func (it *MethodsMapWrappers) FilterWithLimit(
	limit int,
	filterFunc MethodFilterFunc,
) *MethodsMapWrappers {
	length := refinternal.Utils.MaxLimit(
		it.Length(),
		limit)
	newInstance := New.MethodsMapWrappers.empty(it.Length() / 3)

	collectedItems := 0
	for _, name := range it.orderedMethods {
		methodWrapper := it.GetMethod(name)
		isTake, isBreak := filterFunc(methodWrapper)

		if isTake {
			newInstance.add(methodWrapper)
			collectedItems++
		}

		if isBreak {
			return newInstance
		}

		if collectedItems >= length {
			return newInstance
		}
	}

	return newInstance
}

func (it *MethodsMapWrappers) IsEqual(another *MethodsMapWrappers) bool {
	if it == nil && another == nil {
		return true
	}

	if it == nil || another == nil {
		return false
	}

	if it.Length() != another.Length() {
		return false
	}

	return it.IsEqualItems(another.items)
}

func (it *MethodsMapWrappers) IsEqualItems(anotherMap map[string]*MethodWrapper) bool {
	if it == nil && anotherMap == nil {
		return true
	}

	if it == nil || anotherMap == nil {
		return false
	}

	if it.Length() != len(anotherMap) {
		return false
	}

	for name, item := range it.items {
		anotherItem := anotherMap[name]
		hasMismatch := !item.IsEqual(anotherItem)

		if hasMismatch {
			return false
		}
	}

	return true
}

func (it *MethodsMapWrappers) JsonString() string {
	if it.IsEmpty() {
		return "[]"
	}

	jsonBytes, err := json.Marshal(it.items)

	if err != nil {
		panic(err)
	}

	return string(jsonBytes)
}

func (it *MethodsMapWrappers) MethodNamesJsonString() string {
	if it.IsEmpty() {
		return "[]"
	}

	jsonBytes, err := json.Marshal(it.orderedMethods)

	if err != nil {
		panic(err)
	}

	return string(jsonBytes)
}

func (it *MethodsMapWrappers) MethodNames() []string {
	if it.IsEmpty() {
		return []string{}
	}

	return it.orderedMethods
}

func (it *MethodsMapWrappers) Dispose() {
	if it == nil {
		return
	}

	it.items = nil
	it.orderedMethods = nil
}

func (it *MethodsMapWrappers) Clone() MethodsMapWrappers {
	return *it.ClonePtr()
}

func (it *MethodsMapWrappers) ClonePtr() *MethodsMapWrappers {
	if it == nil {
		return nil
	}

	newInstance := New.MethodsMapWrappers.empty(it.Length())

	for _, name := range it.orderedMethods {
		methodWrapper := it.GetMethod(name)
		newInstance.addMethod(methodWrapper.Method)
	}

	return newInstance
}
