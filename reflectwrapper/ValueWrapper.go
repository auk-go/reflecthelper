package reflectwrapper

import (
	"encoding/json"
	"fmt"
	"reflect"
	"sort"

	"gitlab.com/auk-go/reflecthelper/internal/refinternal"
	"gitlab.com/auk-go/reflecthelper/isreflect"
	"gitlab.com/auk-go/reflecthelper/reflectkind"
	"gitlab.com/auk-go/reflecthelper/reflectmodel"
)

type ValueWrapper struct {
	rawItem            interface{}
	rv                 reflect.Value
	rvType             *reflect.Type // must reduce the type to use it further
	kind               reflect.Kind
	enhanceKind        reflectkind.Enhance
	fields             *StructFieldsMap
	methodsMapWrappers *MethodsMapWrappers
	fieldsMap          *map[string]*reflect.StructField
	methodsMap         *map[string]*reflect.Method
	model              *reflectmodel.ReflectValue
	fieldNames         []string
	methodNames        []string
}

// Type
//
//	Cannot use ptr type to get NumFields or anything,
//	thus always reduce it to cache and use from there.
func (it *ValueWrapper) Type() reflect.Type {
	if it == nil {
		return nil
	}

	if it.rvType != nil {
		return *it.rvType
	}

	rvType := refinternal.
		Looper.
		ReducePointerRvDefault(it.rv).
		FinalReflectVal.
		Type()

	it.rvType = &rvType

	return *it.rvType
}

func (it *ValueWrapper) Kind() reflect.Kind {
	if it == nil {
		return reflect.Invalid
	}

	return it.kind
}

func (it *ValueWrapper) EnhanceKind() reflectkind.Enhance {
	if it == nil {
		return reflectkind.Invalid
	}

	return it.enhanceKind
}

func (it *ValueWrapper) StructFieldsMap() *StructFieldsMap {
	if it == nil {
		return nil
	}

	if it.fields != nil {
		return it.fields
	}

	it.fields = New.
		StructFieldsMap.
		CreateUsingRv(it.rv)

	return it.fields
}

func (it *ValueWrapper) MethodsMapWrappers() *MethodsMapWrappers {
	if it == nil {
		return nil
	}

	if it.methodsMapWrappers != nil {
		return it.methodsMapWrappers
	}

	it.methodsMapWrappers = New.
		MethodsMapWrappers.
		Create(it.rawItem)

	return it.methodsMapWrappers
}

func (it *ValueWrapper) MethodsMap() map[string]*reflect.Method {
	if it == nil {
		return nil
	}

	if it.methodsMap != nil {
		return *it.methodsMap
	}

	methodsMap, err := refinternal.Looper.MethodsMapRv(it.rv)

	if err != nil {
		panic(err)
	}

	it.methodsMap = &methodsMap

	return *it.methodsMap
}

func (it *ValueWrapper) FieldsMap() map[string]*reflect.StructField {
	if it == nil {
		return nil
	}

	if it.fieldsMap != nil {
		return *it.fieldsMap
	}

	fieldsMap, err := refinternal.Looper.FieldsMapRv(it.rv)

	if err != nil {
		panic(err)
	}

	it.fieldsMap = &fieldsMap

	return *it.fieldsMap
}

func (it *ValueWrapper) GetMissingMethodNames(
	methodNames ...string,
) (missingNames []string) {
	if it == nil || it.MethodsCount() == 0 {
		return methodNames
	}

	methodsMap := it.MethodsMap()

	for _, name := range methodNames {
		_, has := methodsMap[name]

		if !has {
			missingNames = append(missingNames, name)
		}
	}

	return missingNames
}

func (it *ValueWrapper) GetMissingFieldsNames(
	fieldNames ...string,
) (missingNames []string) {
	if it == nil || it.FieldsCount() == 0 {
		return fieldNames
	}

	fieldsMap := it.FieldsMap()

	for _, name := range fieldNames {
		_, has := fieldsMap[name]

		if !has {
			missingNames = append(missingNames, name)
		}
	}

	return missingNames
}

func (it *ValueWrapper) IsPublic() bool {
	if it == nil {
		return false
	}

	return it.Type().PkgPath() == ""
}

func (it *ValueWrapper) IsPrivate() bool {
	if it == nil {
		return false
	}

	return it.Type().PkgPath() != ""
}

func (it *ValueWrapper) IsEmptyFields() bool {
	if it == nil {
		return false
	}

	return it.FieldsCount() == 0
}

func (it *ValueWrapper) HasAnyFields() bool {
	if it == nil {
		return false
	}

	return it.FieldsCount() > 0
}

func (it *ValueWrapper) Raw() interface{} {
	if it == nil {
		return nil
	}

	return it.rawItem
}

func (it *ValueWrapper) IsTypeOf(t reflect.Type) bool {
	if it == nil {
		return false
	}

	return it.Type() == t
}

func (it *ValueWrapper) TypeName() string {
	if it == nil {
		return ""
	}

	return it.Type().String()
}

func (it *ValueWrapper) GetMethod(name string) *reflect.Method {
	if it == nil {
		return nil
	}

	method, has := it.Type().MethodByName(name)

	if has {
		return &method
	}

	return nil
}

func (it *ValueWrapper) GetMethodWrapper(name string) *MethodWrapper {
	method := it.GetMethod(name)

	if method == nil {
		return nil
	}

	return New.MethodWrapper.Create(*method)
}

func (it *ValueWrapper) GetField(name string) *reflect.StructField {
	if it == nil {
		return nil
	}

	field, has := it.Type().FieldByName(name)

	if has {
		return &field
	}

	return nil
}

func (it *ValueWrapper) HasMethod(name string) bool {
	if it == nil {
		return false
	}

	_, has := it.MethodsMap()[name]

	return has
}

func (it *ValueWrapper) HasField(name string) bool {
	if it == nil {
		return false
	}

	_, has := it.FieldsMap()[name]

	return has
}

func (it *ValueWrapper) FieldNames() (names []string) {
	if it == nil {
		return nil
	}

	if it.IsEmptyFields() {
		return []string{}
	}

	if len(it.fieldNames) > 0 {
		return it.fieldNames
	}

	// generate
	fieldNames, err := refinternal.Looper.FieldNamesRv(it.rv)

	if err != nil {
		panic(err)
	}

	it.fieldNames = fieldNames

	return it.fieldNames
}

func (it *ValueWrapper) MethodNames() (names []string) {
	if it == nil {
		return nil
	}

	for name, _ := range it.MethodsMap() {
		names = append(names, name)
	}

	return names
}

func (it *ValueWrapper) CallMethod(
	name string,
	args ...interface{},
) interface{} {
	if it == nil {
		panic("cannot execute - method - " + name)
	}

	methodWrapper := it.GetMethodWrapper(name)

	if methodWrapper == nil {
		panic(name + " - method - is nil")
	}

	return methodWrapper.Invoke(args...)
}

func (it *ValueWrapper) FieldsCount() int {
	if it == nil {
		return -1
	}

	return it.Type().NumField()
}

func (it *ValueWrapper) MethodsCount() int {
	if it == nil {
		return -1
	}

	return it.rv.NumMethod()
}

func (it *ValueWrapper) JsonModel() interface{} {
	if it == nil {
		return nil
	}

	if it.model != nil {
		return it.model
	}

	fieldNames := it.FieldNames()
	methodNames := it.MethodNames()

	sort.Strings(fieldNames)
	sort.Strings(methodNames)

	model := reflectmodel.ReflectValue{
		TypeName:     it.TypeName(),
		FieldsNames:  fieldNames,
		MethodsNames: methodNames,
		RawData:      it.rawItem,
	}

	it.model = &model

	return it.model
}

func (it *ValueWrapper) JsonString() string {
	if it == nil {
		return ""
	}

	toModel := it.JsonModel()

	if toModel == nil {
		return ""
	}

	toJsonBytes, err := json.Marshal(it.model)

	if err != nil {
		panic(err)
	}

	return string(toJsonBytes)
}

func (it *ValueWrapper) IsPrivateField(fieldName string) bool {
	if it == nil || it.IsInvalid() {
		return false
	}

	structField, has := it.Type().FieldByName(fieldName)

	if !has {
		return false
	}

	return structField.PkgPath != ""
}

func (it *ValueWrapper) IsPublicField(fieldName string) bool {
	if it == nil || it.IsInvalid() {
		return false
	}

	structField, has := it.Type().FieldByName(fieldName)

	if !has {
		return false
	}

	return structField.PkgPath == ""
}

func (it *ValueWrapper) GetRawField(fieldName string) *reflect.StructField {
	if it == nil || it.IsInvalid() {
		return nil
	}

	structField, has := it.Type().FieldByName(fieldName)

	if has {
		return &structField
	}

	return nil
}

func (it *ValueWrapper) IsPointer() bool {
	return it.kind == reflect.Ptr
}

func (it *ValueWrapper) IsNotPointer() bool {
	return it.kind != reflect.Ptr
}

func (it *ValueWrapper) IsValueType() bool {
	return it.kind != reflect.Ptr
}

func (it *ValueWrapper) IsKind(kind reflect.Kind) bool {
	return it.kind == kind
}

func (it *ValueWrapper) IsNotKind(kind reflect.Kind) bool {
	return it.kind != kind
}

func (it *ValueWrapper) IsSlice() bool {
	return it.kind == reflect.Slice
}

func (it *ValueWrapper) IsSliceOrArray() bool {
	return it.kind == reflect.Slice || it.kind == reflect.Array
}

func (it *ValueWrapper) IsStruct() bool {
	return it.kind == reflect.Struct
}

func (it *ValueWrapper) IsInvalid() bool {
	return it == nil || !it.rv.IsValid()
}

func (it *ValueWrapper) IsValueNull() bool {
	return it == nil || it.IsInvalid() || isreflect.Null(it.rv)
}

func (it *ValueWrapper) IsPrimitive() bool {
	return isreflect.Primitive(it.kind)
}

func (it *ValueWrapper) IsMap() bool {
	return it.kind == reflect.Map
}

func (it *ValueWrapper) ReducePointerValue() interface{} {
	if it.IsPointer() {
		return it.rv.Elem().Interface()
	}

	return it.rv.Interface()
}

func (it *ValueWrapper) ValueIntegerDefault() int {
	return it.ValueInteger(0)
}

func (it *ValueWrapper) ValueInteger(defaultVal int) int {
	valInf := it.ReducePointerValue()
	valInt, isOkay := valInf.(int)

	if isOkay {
		return valInt
	}

	return defaultVal
}

func (it *ValueWrapper) ValueStringDefault() string {
	return it.ValueString("")
}

func (it *ValueWrapper) ValueString(defaultString string) string {
	valInf := it.ReducePointerValue()
	valueCasted, isOkay := valInf.(string)

	if isOkay {
		return valueCasted
	}

	return defaultString
}

func (it *ValueWrapper) ConvValueString() string {
	val := it.ValueInterface()

	if val == nil {
		return ""
	}

	return fmt.Sprintf(
		"%v",
		it.ValueInterface())
}

func (it *ValueWrapper) ValueStrings() []string {
	valInf := it.ReducePointerValue()
	valueCasted, isOkay := valInf.([]string)

	if isOkay {
		return valueCasted
	}

	return nil
}

func (it *ValueWrapper) ValueInterface() interface{} {
	return GetPublicValue(it.rv)
}

func (it *ValueWrapper) String() string {
	return it.ConvValueString()
}
