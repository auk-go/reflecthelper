package reflectwrapper

import (
	"reflect"

	"gitlab.com/auk-go/reflecthelper/internal/refinternal"
	"gitlab.com/auk-go/reflecthelper/reflectmodel"
)

type newMethodWrapperCreator struct{}

func (it *newMethodWrapperCreator) Invalid() *MethodWrapper {
	return &MethodWrapper{}
}

func (it *newMethodWrapperCreator) Create(
	method reflect.Method,
) *MethodWrapper {
	return &MethodWrapper{
		MethodReflectValue: method.Func,
		Method:             method,
		Name:               method.Name,
		Index:              method.Index,
		IsValid:            method.Name != "",
	}
}

// Any
//
// should pass a method
//
// Reference:
//
//	https://stackoverflow.com/a/63245652
func (it *newMethodWrapperCreator) Any(
	anyMethod interface{},
) *MethodWrapper {
	switch v := anyMethod.(type) {
	case reflect.Method:
		return it.Create(v)
	default:
		return it.Invalid()
	}
}

// MethodInStruct
//
// should pass a method
//
// Reference:
//
//	https://stackoverflow.com/a/63245652
func (it *newMethodWrapperCreator) MethodInStruct(
	anyStruct interface{},
	methodName string,
) *MethodWrapper {
	rv := reflect.ValueOf(anyStruct)
	methodRv := rv.MethodByName(methodName)
	switch v := methodRv.Interface().(type) {
	case reflect.Method:
		return it.Create(v)
	default:
		return it.Invalid()
	}
}

func (it *newMethodWrapperCreator) AllStructMethodsExcept(
	anyStruct interface{},
	exceptMethods ...string,
) []*MethodWrapper {
	list := make([]*MethodWrapper, 0, 5)
	exceptNamesMap := map[string]bool{}
	if len(exceptMethods) > 0 {
		for _, methodName := range exceptMethods {
			exceptNamesMap[methodName] = true
		}
	}

	err := refinternal.Looper.MethodsFor(
		anyStruct,
		func(
			totalMethodsCount int,
			method *reflectmodel.MethodProcessor,
		) (err error) {
			if exceptNamesMap[method.Name] {
				// skip it
				return nil
			}

			list = append(list, it.Create(method.ReflectMethod))

			return nil
		})

	if err != nil {
		panic(err)
	}

	return list
}

func (it *newMethodWrapperCreator) All(
	anyStruct interface{},
) []*MethodWrapper {
	return it.AllStructMethodsExcept(anyStruct)
}

func (it *newMethodWrapperCreator) CreateWithFilter(
	anyStruct interface{},
	isFilter func(methodWrapper *MethodWrapper) (isTake bool),
) []*MethodWrapper {
	list := make([]*MethodWrapper, 0, 5)
	err := refinternal.Looper.MethodsFor(
		anyStruct,
		func(
			totalMethodsCount int,
			method *reflectmodel.MethodProcessor,
		) (err error) {
			newMethodWrapper := it.Create(method.ReflectMethod)

			if isFilter(newMethodWrapper) {
				list = append(list, newMethodWrapper)
			}

			return nil
		})

	if err != nil {
		panic(err)
	}

	return list
}

func (it *newMethodWrapperCreator) SpecificMethods(
	anyStruct interface{},
	methodNames ...string,
) []*MethodWrapper {
	list := make([]*MethodWrapper, 0, 5)
	onlyNames := map[string]bool{}
	if len(methodNames) > 0 {
		for _, methodName := range methodNames {
			onlyNames[methodName] = true
		}
	}

	err := refinternal.Looper.MethodsFor(
		anyStruct,
		func(
			totalMethodsCount int,
			method *reflectmodel.MethodProcessor,
		) (err error) {
			if onlyNames[method.Name] {
				// only add if in the list
				list = append(list, it.Create(method.ReflectMethod))
			}

			return nil
		})

	if err != nil {
		panic(err)
	}

	return list
}
