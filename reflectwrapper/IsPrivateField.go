package reflectwrapper

import "reflect"

func IsPrivateField(field reflect.StructField) bool {
	return field.PkgPath != "" // empty for public field
}
