package reflectwrapper

import "reflect"

func GetRawField(anyStruct interface{}, fieldName string) reflect.StructField {
	rVal := reflect.ValueOf(anyStruct)
	f, _ := rVal.Type().FieldByName(fieldName)

	return f
}
