package reflectwrapper

import (
	"gitlab.com/auk-go/reflecthelper/reflectmodel"
)

func GetFieldModel(anyStruct interface{}, fieldName string) reflectmodel.FieldProcessor {
	field := GetRawField(anyStruct, fieldName)

	return reflectmodel.FieldProcessor{
		Name:      field.Name,
		Field:     field,
		FieldType: field.Type,
	}
}
