package reflectwrapper

import "reflect"

func GetPublicValue(rv reflect.Value) interface{} {
	if !rv.IsValid() {
		return nil
	}

	k := rv.Kind()

	if k == reflect.Ptr || k == reflect.Interface {
		return rv.Elem().Interface()
	}

	return rv.Interface()
}
