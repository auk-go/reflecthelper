package reflectwrapper

type newCreator struct {
	StructFieldsMap    *newStructFieldsMapCreator
	Wrapper            *newWrapperCreator
	SimpleWrapper      *newSimpleWrapperCreator
	SimpleHashset      *newSimpleHashsetCreator
	MethodWrapper      *newMethodWrapperCreator
	MethodsMapWrappers *newMethodsMapWrappersCreator
	ValueWrapper       *newValueWrapperCreator
}
