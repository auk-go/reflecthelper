package reflecttype

import "math"

func ToIdentifier(i float64) Identifier {
	if i > math.MaxUint8 || i < 0 {
		return unknownType
	}

	return Identifier(math.Trunc(i))
}
