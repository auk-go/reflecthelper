package reflecttype

type Identifier byte

// identifier types
const (
	dummyStart Identifier = iota
	MapType
	StructType
	SliceType
	PrimitiveType
	dummyEnd
	unknownType
)

func (typ Identifier) String() string {
	if !IsTypeValid(typ) {
		return genericTypeIdentifierStrings[unknownType]
	}

	return genericTypeIdentifierStrings[typ]
}
