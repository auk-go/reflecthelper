package reflecttype

// GenericPrimitive wraps primitive value
type GenericPrimitive struct {
	Value          interface{}
	ReflectionType string
}
