package reflecttype

// GenericStruct represents struct in map[field]interface{} form
type GenericStruct struct {
	ResultMap      *map[string]*Arbitrary
	ReflectionType string
}
