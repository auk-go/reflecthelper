package reflecttype

// GenericSlice represents any type of slice in []interface{} form
type GenericSlice struct {
	ResultSlice    *[]*Arbitrary
	ReflectionType string
}
