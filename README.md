![](Logo)

# Reflecthelper

Reflecton related everything, you want to get struct properties, method, invoke dynamically.
Use this package. The best reflect package ever.

If not the best let me know.

## Git Clone

`git clone https://gitlab.com/auk-go/reflecthelper.git`

## Installation

`go get gitlab.com/auk-go/reflecthelper`

## Why `reflecthelper`?

## Examples

```go
package main

func main() {
    type SomeStruct struct {
        SomeInt         int
        SomeStringPtr   *string
        SomeSlice       *[]int
    }

    type SomeWrapperStruct struct {
        PtrToSomeStruct *SomeStruct
    }

    myMsg := "Hello World"
    var destStruct  SomeWrapperStruct
    srcStruct := SomeWrapperStruct{
        &SomeStruct{
            17,
            &myMsg,
            &[]int{1, 2, 3},
        },
    }

    err := reflecthelper.Get(&destStruct, srcStruct)

    if err != nil {
        log.Fatal(err)
    }

    fmt.Println(reflect.DeepEqual(srcStruct, destStruct))
}
```

## Acknowledgement

Any other packages used

## Links

* [go - Using reflect, how do you set the value of a struct field? - Stack Overflow](https://stackoverflow.com/questions/6395076/using-reflect-how-do-you-set-the-value-of-a-struct-field/6396678#6396678)
* [Learning to Use Go Reflection. Post 5 in a Series on Go | by Jon Bodner | Capital One Tech | Medium](https://medium.com/capital-one-tech/learning-to-use-go-reflection-822a0aed74b7)
* [Reflect in Golang - Scaler Topics](https://www.scaler.com/topics/golang/reflect-in-golang)
  * https://stackoverflow.com/a/59831691

## Issues

- [Create your issues](https://gitlab.com/auk-go/reflecthelper/-/issues)

## Notes

## Contributors

- [Alim Ul Karim](https://www.google.com/search?q=Alim+Ul+Karim)

## License

[MIT License](/LICENSE)
