package reflectptrs

import (
	"reflect"
)

// ReduceToValueUsingFunc
//
// isKindSkipFunc return true when want to skip for reduction
// Examples can be
//  - For ptr reduce return true for isKindSkipFunc -> kind == reflect.Ptr
func ReduceToValueUsingFunc(
	input interface{},
	isKindSkipFunc func(kind reflect.Kind) bool,
) *ReducedResult {
	initialValue := reflect.ValueOf(input)

	return ReflectValReduceToValUsingFunc(
		&initialValue,
		isKindSkipFunc)
}
