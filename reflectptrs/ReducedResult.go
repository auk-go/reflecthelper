package reflectptrs

import "reflect"

type ReducedResult struct {
	InputRawReflectValue *reflect.Value
	ReducedReflectValue  *reflect.Value
	InitialKind          reflect.Kind
	ReducedKind          reflect.Kind
	Error                error
	IsReducedSuccess     bool
}
