package reflectptrs

import (
	"reflect"
)

// ReduceToValue
//
// input can be ptr, interface or struct => struct.
//
// Best input to be struct. Returns struct, if it was not struct then returns error
func ReduceToValue(input interface{}) *ReducedResult {
	initialValue := reflect.ValueOf(input)

	return ReflectValReduceToVal(&initialValue)
}
