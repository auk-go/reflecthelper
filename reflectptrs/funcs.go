package reflectptrs

import "reflect"

var (
	isPtrInterfaceSliceSkipFunc = func(kind reflect.Kind) bool {
		return kind == reflect.Ptr ||
			kind == reflect.Interface ||
			kind == reflect.Slice
	}
)
