package reflectcasttests

type (
	u1 struct {
		i int
		s string
	}

	E1 struct {
		I int
		S string
	}

	u2 struct {
		ip  *int
		arr [3]int
	}

	E2 struct {
		Ip  *int
		Arr [3]int
	}
)
