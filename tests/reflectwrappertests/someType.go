package reflectwrappertests

import (
	"errors"
	"strconv"
)

type someType struct {
	F1 string
	F2 int
}

func (it *someType) GetMethod1(x int) string {
	return it.F1 + strconv.Itoa(x)
}

func (it *someType) GetMethod2() int {
	return it.F2
}

func (it someType) SomeErr(additionalErr string) error {
	return errors.New("some sample error " + additionalErr)
}
