package reflectwrappertests

import (
	"encoding/json"
	"fmt"
	"strconv"
	"testing"

	"github.com/smartystreets/goconvey/convey"
	"gitlab.com/auk-go/reflecthelper/reflectfields"
	"gitlab.com/auk-go/reflecthelper/reflectmodel"
	"gitlab.com/auk-go/reflecthelper/reflectwrapper"
)

// Test_GetFieldsMap
//
// https://t.ly/_LriJ
func Test_GetFieldsMap(t *testing.T) {
	somePublicFields := someType{}
	allPublicFields := someAllPublicType{}

	somePubFieldsReflect := reflectfields.GetFieldsMap(somePublicFields)
	allPubFieldsReflect := reflectfields.GetFieldsMap(allPublicFields)

	fieldNames := []string{
		"A",
		"B",
	}

	convey.Convey("GetFieldsMap", t, func() {
		convey.Convey("Some public fields should yield only public fields not private fields", func() {
			missing := make([]string, 0)
			for _, name := range []string{"F1"} {
				_, has := somePubFieldsReflect[name]

				if !has {
					missing = append(missing, name)
				}
			}

			toMsg, _ := json.Marshal(missing)
			convey.So(string(toMsg), convey.ShouldEqual, "[]")
		})

		convey.Convey("All public struct should yield all the fields", func() {
			missingNames := make([]string, 0)
			for _, name := range fieldNames {
				_, has := allPubFieldsReflect[name]

				if !has {
					missingNames = append(missingNames, name)
				}
			}

			toMsg, _ := json.Marshal(missingNames)
			convey.So(string(toMsg), convey.ShouldEqual, "[]")
		})
	})
}

// Test_GetFieldsMap
//
// https://t.ly/_LriJ
func Test_NewMethodWrapperAll(t *testing.T) {
	item := reflectmodel.FieldProcessor{}

	convey.Convey("MethodWrapper All", t, func() {
		convey.Convey("All methods found", func() {
			// Arrange
			methodWrappers := reflectwrapper.New.MethodWrapper.All(item)
			methodsMap := reflectwrapper.New.MethodsMapWrappers.CreateUsingWrappers(methodWrappers)
			toMsg := methodsMap.MethodNamesJsonString()

			// Assert
			convey.So(toMsg, convey.ShouldEqual, "[\"IsFieldKind\",\"IsFieldType\"]")
		})
	})
}

// Test_GetFieldsMap
//
// https://t.ly/_LriJ
func Test_MethodWrapperInvoke(t *testing.T) {
	s := &someType{
		F1: "F1 val - combine with - ",
		F2: 2,
	}

	type names struct {
		GetMethod1 string
		GetMethod2 string
		SomeErr    string
	}

	methodNames := names{
		GetMethod1: "GetMethod1",
		GetMethod2: "GetMethod2",
		SomeErr:    "SomeErr",
	}

	convey.Convey("MethodWrapper Invoking Tests", t, func() {
		convey.Convey("Invoke Any Method", func() {
			// Arrange
			methodsMap := reflectwrapper.New.MethodsMapWrappers.Create(s)
			toMsg := methodsMap.MethodNamesJsonString()

			// Assert
			convey.So(toMsg, convey.ShouldEqual, "[\"GetMethod1\",\"GetMethod2\",\"SomeErr\"]")

			header1 := "Invoking - " + methodNames.GetMethod1 + " - should yield number"
			convey.Convey(header1, func() {
				// Arrange
				number := 50
				result := methodsMap.
					GetMethod(methodNames.GetMethod1).
					GetFirstResponseOfInvoke(s, number)

				// Assert
				convey.So(result, convey.ShouldEqual, "F1 val - combine with - "+strconv.Itoa(number))
			})

			header2 := "Invoking - " + methodNames.GetMethod2 + " - should yield string"
			convey.Convey(header2, func() {
				// Arrange
				result := methodsMap.
					GetMethod(methodNames.GetMethod2).
					GetFirstResponseOfInvoke(s)

				// Assert
				convey.So(result, convey.ShouldEqual, s.F2)
			})

			header3 := "Invoking - " + methodNames.SomeErr + " - should yield error"
			convey.Convey(header3, func() {
				// Arrange
				result := methodsMap.
					GetMethod(methodNames.SomeErr).
					InvokeError(s, "alim new error message")

				// Assert
				convey.So(result, convey.ShouldNotBeNil)
				convey.So(result.Error(), convey.ShouldEqual, "some sample error alim new error message")
			})
		})
	})
}

// Test_GetFieldsMap
//
// https://t.ly/_LriJ
func Test_MethodWrapperInvokeError(t *testing.T) {
	s := &someType{
		F1: "F1 val - combine with - ",
		F2: 2,
	}

	type names struct {
		GetMethod1      string
		GetMethod2      string
		WrongMethodName string
		SomeErr         string
	}

	methodNames := names{
		GetMethod1:      "GetMethod1",
		GetMethod2:      "GetMethod2",
		WrongMethodName: "WrongMethodName",
		SomeErr:         "SomeErr",
	}

	// Arrange
	methodsMap := reflectwrapper.New.MethodsMapWrappers.Create(s)

	convey.Convey("MethodWrapper Invoking Wrong Method or Wrong Args", t, func() {

		convey.Convey("Invoke Any Method", func() {
			header2 := "Invoking - " + methodNames.WrongMethodName + " - is not found in the dictionary"

			convey.Convey(header2, func() {
				// Arrange
				result := methodsMap.GetMethod(methodNames.WrongMethodName)

				// Assert
				convey.So(result, convey.ShouldBeNil)
			})

			header3 := "Invoking - " + methodNames.SomeErr + " - should yield error"
			convey.Convey(header3, func() {
				// Arrange
				result := methodsMap.
					GetMethod(methodNames.SomeErr).
					InvokeError(s, "alim new error message")

				// Assert
				convey.So(result, convey.ShouldNotBeNil)
				convey.So(result.Error(), convey.ShouldEqual, "some sample error alim new error message")
			})
		})
	})
}

func Test_ValueWrapper(t *testing.T) {
	s := &someType{
		F1: "F1 val - combine with - ",
		F2: 2,
	}

	// Arrange
	valueWrapper := reflectwrapper.New.ValueWrapper.Create(s)
	convey.Convey("Value Wrapper Verification", t, func() {
		convey.Convey("String output verification", func() {
			// Arrange
			toString := valueWrapper.JsonString()
			expected := "{\"TypeName\":\"reflectwrappertests.someType\",\"FieldsNames\":[\"F1\",\"F2\"],\"MethodsNames\":[\"GetMethod1\",\"GetMethod2\",\"SomeErr\"],\"RawData\":{\"F1\":\"F1 val - combine with - \",\"F2\":2}}"

			fmt.Println(toString)
			// Assert
			convey.So(toString, convey.ShouldEqual, expected)
		})

		convey.Convey("MethodMaps verification", func() {
			// Arrange
			expected := []string{
				"GetMethod1",
				"GetMethod2",
				"SomeErr",
			}

			// Act
			missing := valueWrapper.GetMissingMethodNames(expected...)

			// Assert
			convey.So(missing, convey.ShouldBeEmpty)
		})

		convey.Convey("FieldsMap verification", func() {
			// Arrange
			expected := []string{
				"F1",
				"F2",
			}

			// Act
			missing := valueWrapper.GetMissingFieldsNames(expected...)

			// Assert
			convey.So(missing, convey.ShouldBeEmpty)
		})
	})
}
