package reflectfieldtests

import (
	"testing"

	"github.com/smartystreets/goconvey/convey"
	"gitlab.com/auk-go/reflecthelper/reflectwrapper"
)

// Test_IsPublicField
//
// https://t.ly/_LriJ
func Test_IsPublicField(t *testing.T) {
	type SomeType struct {
		A string
		b int
	}

	virtualStruct := SomeType{
		A: "Some public field",
		b: -500,
	}

	structFieldMap := reflectwrapper.New.StructFieldsMap.Create(virtualStruct)
	fields := structFieldMap.GetRawFieldsMapMust("A", "b")

	isAPublicField := reflectwrapper.IsPublicField(*fields["A"])
	isbPublicField := reflectwrapper.IsPublicField(*fields["b"])

	convey.Convey("IsPublicField", t, func() {
		convey.Convey("A field in Struct is public field", func() {
			convey.So(isAPublicField, convey.ShouldBeTrue)
		})

		convey.Convey("B field in Struct is private field", func() {
			convey.So(isbPublicField, convey.ShouldBeFalse)
		})
	})
}

// Test_IsAllFieldPublic
//
// https://t.ly/_LriJ
func Test_IsAllFieldPublic(t *testing.T) {
	type SomeType struct {
		A string
		b int
	}

	type SomeAllPublicType struct {
		A string
		B int
	}

	somePublicFields := SomeType{}
	allPublicFields := SomeAllPublicType{}

	somePubFieldsReflect := reflectwrapper.New.StructFieldsMap.RawFieldsMap(somePublicFields)
	allPubFieldsReflect := reflectwrapper.New.StructFieldsMap.RawFieldsMap(allPublicFields)

	isSomePubFieldsReflectTrue := reflectwrapper.IsAllMapFieldPublic(somePubFieldsReflect)
	isAllPubFieldsReflectTrue := reflectwrapper.IsAllMapFieldPublic(allPubFieldsReflect)

	convey.Convey("IsAllMapFieldPublic", t, func() {
		convey.Convey("All public fields", func() {
			convey.So(isAllPubFieldsReflectTrue, convey.ShouldBeTrue)
		})

		convey.Convey("Some public fields should yield false", func() {
			convey.So(isSomePubFieldsReflectTrue, convey.ShouldBeFalse)
		})
	})
}
