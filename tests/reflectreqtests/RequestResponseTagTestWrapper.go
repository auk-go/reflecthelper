package reflectreqtests

import "gitlab.com/auk-go/reflecthelper/reflectreq"

type RequestResponseTagTestWrapper struct {
	Header  string
	Request reflectreq.ChildFieldTagsExpectationRequest
}
