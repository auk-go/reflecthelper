package reflectreqtests

type (
	t1 struct {
		a int
		b string
		k *int
		C struct {
			p []int
			Q *int `json:"blah" xml:"more_blah"`
		}
	}

	t2 struct {
		A struct {
			B string
			C struct {
				D struct {
					d1 int
					d2 string
					d3 string
					d4 []string
				}
				E string
			}
		}
	}
)
