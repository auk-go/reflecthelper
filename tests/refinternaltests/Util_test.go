package refinternaltests

import (
	"testing"

	"github.com/smartystreets/goconvey/convey"
	"gitlab.com/auk-go/reflecthelper/internal/refinternal"
)

// Test_Util_IsReflectTypeMatch
//
// Verify types verification
func Test_Util_IsReflectTypeMatch(t *testing.T) {
	isChecker := refinternal.Utils.IsReflectTypeMatchAny

	t1 := someType{}
	t2 := someAllPublicType{}
	t3 := someType{}

	convey.Convey("IsReflectTypeMatch", t, func() {
		convey.Convey("Left type and Right type - Match - No Error - Same Object", func() {
			isOkay, err := isChecker(t1, t1)

			convey.So(
				err,
				convey.ShouldBeNil)
			convey.So(
				isOkay,
				convey.ShouldBeTrue)
		})

		convey.Convey("Left type and Right type - Match - No Error - Different Object", func() {
			isOkay, err := isChecker(t1, t3)

			convey.So(
				err,
				convey.ShouldBeNil)
			convey.So(
				isOkay,
				convey.ShouldBeTrue)
		})

		convey.Convey("Left type and Right type Mismatch Error Verify", func() {
			isOkay, err := isChecker(t1, t2)

			convey.So(
				err,
				convey.ShouldNotBeNil)
			convey.So(
				err.Error(),
				convey.ShouldEqual,
				"Left Type (someType) != Right Type (someAllPublicType)")
			convey.So(
				isOkay,
				convey.ShouldBeFalse)
		})
	})
}

// Test_Util_IsReflectTypeMatch
//
// Verify types verification
func Test_Util_VerifyReflectTypes(t *testing.T) {
	isChecker := refinternal.Utils.VerifyReflectTypesAny

	t1 := someType{}
	t2 := someAllPublicType{}
	t3 := someType{}
	t4 := someType{}

	leftTypes := []interface{}{
		t1,
		t2,
		t3,
		t4,
	}

	rightSameTypes := []interface{}{
		t1,
		t2,
		t3,
		t1,
	}

	rightDiffTypes := []interface{}{
		t2,
		t2,
		t3,
		t2,
	}

	convey.Convey("IsReflectTypeMatch", t, func() {
		convey.Convey("Left type and Right type - Match - No Error - Same Object", func() {
			isOkay, err := isChecker(leftTypes, leftTypes)

			convey.So(
				err,
				convey.ShouldBeNil)
			convey.So(
				isOkay,
				convey.ShouldBeTrue)
		})

		convey.Convey("Left type and Right type - Match - No Error - Different Object", func() {
			isOkay, err := isChecker(leftTypes, rightSameTypes)

			convey.So(
				err,
				convey.ShouldBeNil)
			convey.So(
				isOkay,
				convey.ShouldBeTrue)
		})

		convey.Convey("Left type and Right type Mismatch Error Verify", func() {
			isOkay, err := isChecker(leftTypes, rightDiffTypes)

			// fmt.Print(isOkay, err)

			convey.So(
				err,
				convey.ShouldNotBeNil)
			convey.So(
				err.Error(),
				convey.ShouldEqual,
				"- Index {0} - 1th : Left Type (someType) != Right Type (someAllPublicType)\n"+
					"- Index {3} - 4th : Left Type (someType) != Right Type (someAllPublicType)")
			convey.So(
				isOkay,
				convey.ShouldBeFalse)
		})
	})
}
