package refinternaltests

import (
	"errors"
	"strconv"
)

type someType struct {
	f1 string
	f2 int
}

func (it *someType) GetMethod1(x int) string {
	return it.f1 + strconv.Itoa(x)
}

func (it *someType) GetMethod2() int {
	return it.f2
}

func (it someType) SomeErr(additionalErr string) error {
	return errors.New("some sample error " + additionalErr)
}
