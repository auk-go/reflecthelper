package reflecthelper

import (
	"reflect"

	"gitlab.com/auk-go/reflecthelper/internal/refinternal"
)

type utils struct{}

func (it utils) MaxLimit(currentLength, maxCapacity int) int {
	return refinternal.Utils.MaxLimit(currentLength, maxCapacity)
}

func (it utils) AppendArgs(appendingItem interface{}, args []interface{}) []interface{} {
	return refinternal.Utils.AppendArgs(appendingItem, args)
}

func (it utils) VerifyReflectTypesAny(left, right []interface{}) (isOkay bool, err error) {
	return refinternal.Utils.VerifyReflectTypesAny(left, right)
}

func (it utils) VerifyReflectTypes(left, right []reflect.Type) (isOkay bool, err error) {
	return refinternal.Utils.VerifyReflectTypes(left, right)
}

func (it utils) IsReflectTypeMatch(left, right reflect.Type) (isOkay bool, err error) {
	return refinternal.Utils.IsReflectTypeMatch(left, right)
}

func (it utils) IsReflectTypeMatchAny(left, right interface{}) (isOkay bool, err error) {
	return refinternal.Utils.IsReflectTypeMatchAny(left, right)
}
