package reflectcast

import (
	"reflect"

	"gitlab.com/auk-go/reflecthelper/internal/refinternal"
)

// ToReflectPointer
//
// converts struct or struct pointers reflect value to pointer based reflect value
func ToReflectPointer(rv reflect.Value) (reflect.Value, error) {
	return refinternal.Looper.ToPointerReflectValueRv(rv)
}

// ToReflectPointerAny
//
// converts struct or struct pointers reflect value to pointer based reflect value
func ToReflectPointerAny(anyStruct interface{}) (reflect.Value, error) {
	return refinternal.Looper.ToPointerReflectValue(anyStruct)
}
