package reflectcast

import "reflect"

var (
	Uint8sType   = reflect.ValueOf([]byte{}).Type()
	BytesType    = Uint8sType
	StringsType  = reflect.ValueOf([]string{}).Type()
	StringType   = reflect.ValueOf("").Type()
	IntegersType = reflect.ValueOf([]int{}).Type()
	IntegerType  = reflect.ValueOf(0).Type()
	BooleanType  = reflect.ValueOf(false).Type()
	Int64sType   = reflect.ValueOf([]int64{}).Type()
	Float64sType = reflect.ValueOf([]float64{}).Type()
	BooleansType = reflect.ValueOf([]bool{}).Type()
	AnyType      = reflect.ValueOf([]interface{}{}).Type()
)
