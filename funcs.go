package reflecthelper

import "gitlab.com/auk-go/reflecthelper/reflectmodel"

type (
	MethodProcessorFunc func(
		totalMethodsCount int,
		method *reflectmodel.MethodProcessor,
	) (err error)

	FieldsProcessorFunc func(
		currentField *reflectmodel.FieldProcessor,
	) (err error)
)
