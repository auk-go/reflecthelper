package reflecthelper

type Options struct {
	SkippingFieldsNames        map[string]bool
	IsPanicOnFieldNotPresent   bool
	IsPanicOnFieldTypeMismatch bool
}

func (opt *Options) ContainsFieldName(fieldName string) bool {
	if opt.SkippingFieldsNames == nil {
		return false
	}

	_, isFound := opt.SkippingFieldsNames[fieldName]

	return isFound
}

func (opt *Options) IsSkipField(fieldName string) bool {
	return opt.ContainsFieldName(fieldName)
}
