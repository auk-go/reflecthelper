package reflecthelper

func SetFromToStrict(from, to interface{}, skipFieldNames ...string) error {
	opts := NewReflectGetOptions(
		true, // is panic on field not present
		true, // is panic on field type mismatch
		skipFieldNames...,
	)

	g := newGetter(opts, false)

	// not handling error, because it'll panic immediately
	return g.SetFromTo(from, to)
}
