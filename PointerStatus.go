package reflecthelper

import "reflect"

func PointerStatus(
	toPtr interface{},
) (typeName string, isNull, isPtr bool) {
	typeName = "null pointer, cannot retrieve type"
	currentType := reflect.TypeOf(toPtr)
	isDefined := !isNullCheck(reflect.ValueOf(currentType))

	if isDefined {
		typeName = currentType.String()
		isPtr = currentType.Kind() == reflect.Ptr
		isNull = isNullCheck(reflect.ValueOf(toPtr))
	} else {
		isNull = true
	}

	return typeName, isNull, isPtr
}
